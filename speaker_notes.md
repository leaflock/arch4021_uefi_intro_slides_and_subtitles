Hello and welcome to Arch4021 Introductory UEFI.

I'm Piotr Król, and I will be your trainer during this course. 

What are the goals of this training? 

First, we would like to learn the fundamentals of firmware.

What is firmware, how it works, where is it stored, and some similar basics? 

Then we will talk about UEFI basics like what are the goals of UEFI
specification, what are the essential components of it, and what's the history behind
UEFI specification, then we will explain the architecture of UEFI. To not be too
theoretical at the beginning, we will discuss various implementations of UEFI,
especially reference implementation EDKII.

We will take a look at criticism pointed into UEFI and what are the main
concerns of people criticizing this Specification. 

Then, we will discuss boot flow and all the phases which are related to booting the
the modern platform using UEFI BIOS. 

From the practical point of view, we will learn how to build UEFI
reference implementation, as well as how to debug it.

We will explore how images are built and how they look inside. We will
decompose images that we created to identify components described in PI
specification.

Then we will talk about crucial structures used inside the boot process, as well as
what are the boot phases and how to find them in the boot log? How to recognize
what kind of features they Have.

In the final section of this course, we will discuss UEFI Variables, which are
fundamental to the understanding of UEFI Secure Boot. We will discuss services
and structures used to read and write UEFI Variables.

We will play with types of UEFI Variables, those superficial 
and complex. To understand the level of complexity related to handling
various variables.

You may ask yourself, why should I take this class?

You may be planning a career in firmware development, want to understand how
the firmware works because of your security research or may be
interested in how modern computing devices boot. This course was created to
help you with achieving those goals. It was designed to be
introductory to more complex topics like UEFI Secure Boot, deep dive into UEFI
internals, and hardware hands-on with the ultimate goal of putting together your
own UEFI firmware for hardware devices.

UEFI is one of the possible ways to boot modern computing systems. For example in
Arch4031, we discuss an open-source firmware framework called coreboot (or by some
open-source BIOS). Knowing multiple implementations prepare you for building
more secure and reliable firmware and improves your ability to find
vulnerabilities. Finally, you may feel some satisfaction from understanding the
nitty-gritty ideas and details behind the scenes.

I wish you good luck with the course and hope it will be the first small step
into the fascinating world of hardware initialization and low-level security.
