1
00:00:00,000 --> 00:00:03,959
as we already mentioned you have high

2
00:00:01,860 --> 00:00:06,600
specification defined set of variables

3
00:00:03,959 --> 00:00:09,720
with architecturally defined meaning

4
00:00:06,600 --> 00:00:12,120
those are variables with special EFI

5
00:00:09,720 --> 00:00:13,980
Global variable vendor GUID we can say

6
00:00:12,120 --> 00:00:17,460
that all those special architecturally

7
00:00:13,980 --> 00:00:19,800
defined UEFI variables are in EFI Global

8
00:00:17,460 --> 00:00:22,199
variable namespace in the specification

9
00:00:19,800 --> 00:00:26,039
there are 40 Global variables described

10
00:00:22,199 --> 00:00:28,920
in UEFI specification section 3.3 and in

11
00:00:26,039 --> 00:00:31,500
further sections we will discuss most

12
00:00:28,920 --> 00:00:35,219
most important one we can divide those

13
00:00:31,500 --> 00:00:38,040
for 40 variables in couple groups and

14
00:00:35,219 --> 00:00:40,320
and now I would like to talk a little

15
00:00:38,040 --> 00:00:42,180
bit about those groups first is UEFI

16
00:00:40,320 --> 00:00:44,160
secure boot group and there are a couple

17
00:00:42,180 --> 00:00:47,879
variables there we will not discuss

18
00:00:44,160 --> 00:00:49,680
those in details but we can just like

19
00:00:47,879 --> 00:00:51,600
right now know that those are associated

20
00:00:49,680 --> 00:00:55,379
with UEFI secure boot mechanism

21
00:00:51,600 --> 00:00:58,199
implemented in EDK2 according to file

22
00:00:55,379 --> 00:01:00,840
specification and yeah this topic is

23
00:00:58,199 --> 00:01:03,719
quite big and it will belongs to

24
00:01:00,840 --> 00:01:06,960
Dedicated open security training course

25
00:01:03,719 --> 00:01:09,240
the other group is boot order and

26
00:01:06,960 --> 00:01:11,460
related configuration this is a very

27
00:01:09,240 --> 00:01:14,880
important group responsible for handling

28
00:01:11,460 --> 00:01:16,920
order of device devices used during the

29
00:01:14,880 --> 00:01:18,960
boot process of the platform we will

30
00:01:16,920 --> 00:01:21,659
discuss those variables in in more

31
00:01:18,960 --> 00:01:25,200
details later next group is console

32
00:01:21,659 --> 00:01:28,020
input and output group of variables is

33
00:01:25,200 --> 00:01:31,500
responsible for input and output at UEFI

34
00:01:28,020 --> 00:01:34,140
BIOS level partially thanks to correct

35
00:01:31,500 --> 00:01:36,900
configuration of those variables and of

36
00:01:34,140 --> 00:01:39,900
course drivers behind the scene and we

37
00:01:36,900 --> 00:01:43,079
can use our keyboard or Mouse and we can

38
00:01:39,900 --> 00:01:46,680
see things on output uh when when

39
00:01:43,079 --> 00:01:49,200
dealing with UEFI BIOS menu or dealing

40
00:01:46,680 --> 00:01:53,340
with UEFI shell a driver loading options

41
00:01:49,200 --> 00:01:57,479
group is a group of variables which can

42
00:01:53,340 --> 00:02:00,600
be used to provide options for drivers

43
00:01:57,479 --> 00:02:02,640
loaded during the boot process yes we

44
00:02:00,600 --> 00:02:05,399
can load drivers and we can even

45
00:02:02,640 --> 00:02:07,680
override a previously loaded drivers

46
00:02:05,399 --> 00:02:09,780
during the boot process and those

47
00:02:07,680 --> 00:02:12,239
variables help us deal with the options

48
00:02:09,780 --> 00:02:14,459
provided to those drivers so yeah so

49
00:02:12,239 --> 00:02:17,220
this is this is the same as then

50
00:02:14,459 --> 00:02:19,620
language group is responsible for

51
00:02:17,220 --> 00:02:22,739
supported languages and also for

52
00:02:19,620 --> 00:02:25,440
configurate language that we see in BIOS

53
00:02:22,739 --> 00:02:29,040
setup menu OS firmware features

54
00:02:25,440 --> 00:02:32,040
communication group of variables is very

55
00:02:29,040 --> 00:02:34,260
interesting because it provides means of

56
00:02:32,040 --> 00:02:36,420
communication between firmware and the

57
00:02:34,260 --> 00:02:40,080
operating system and operating system

58
00:02:36,420 --> 00:02:43,080
and firmware about features which are

59
00:02:40,080 --> 00:02:44,700
supported by one or other site use of

60
00:02:43,080 --> 00:02:46,319
those variables may be very very

61
00:02:44,700 --> 00:02:48,959
interesting to security researchers

62
00:02:46,319 --> 00:02:52,319
because those variables have potential

63
00:02:48,959 --> 00:02:55,200
of changing operating system Behavior or

64
00:02:52,319 --> 00:02:58,260
firmware Behavior the next group is UEFI

65
00:02:55,200 --> 00:03:01,860
capsule update group this group is used

66
00:02:58,260 --> 00:03:04,620
by recommended and implemented in EDK2

67
00:03:01,860 --> 00:03:07,019
UEFI firmware update mechanism call it

68
00:03:04,620 --> 00:03:09,420
UEFI capsule update and we will talk a

69
00:03:07,019 --> 00:03:13,200
little bit about those variables later

70
00:03:09,420 --> 00:03:15,200
some variables can can be even used for

71
00:03:13,200 --> 00:03:18,959
for example for storing

72
00:03:15,200 --> 00:03:22,920
ACPI SSDT a table secondary system

73
00:03:18,959 --> 00:03:27,239
descriptor table overlays so we can have

74
00:03:22,920 --> 00:03:30,239
some support to Overlay some ACPI tables

75
00:03:27,239 --> 00:03:33,360
through UEFI variables this may be

76
00:03:30,239 --> 00:03:35,760
helpful in fixing broken ACPI tables

77
00:03:33,360 --> 00:03:38,040
which were provided by BIOS vendor or

78
00:03:35,760 --> 00:03:40,580
maybe helpful in in some special

79
00:03:38,040 --> 00:03:42,840
Hardware like open-ended Hardware

80
00:03:40,580 --> 00:03:46,080
configurations like for development

81
00:03:42,840 --> 00:03:48,480
boards or similar similar Hardware in

82
00:03:46,080 --> 00:03:50,940
that way instead of building dedicated

83
00:03:48,480 --> 00:03:53,040
BIOS for every potential configuration

84
00:03:50,940 --> 00:03:58,560
of that development board we're just

85
00:03:53,040 --> 00:04:00,720
using ACPI SSDT overlies to change or

86
00:03:58,560 --> 00:04:04,080
expose additional interfaces like I

87
00:04:00,720 --> 00:04:07,019
Square C or SPI buses and this is very

88
00:04:04,080 --> 00:04:08,720
useful mechanism in case of open-ended

89
00:04:07,019 --> 00:04:12,299
Hardware there are also a couple other

90
00:04:08,720 --> 00:04:15,840
variables like recovery options or time

91
00:04:12,299 --> 00:04:18,720
related variables and we will briefly

92
00:04:15,840 --> 00:04:21,840
discuss those later in the in further

93
00:04:18,720 --> 00:04:24,720
sections but who may be interested in

94
00:04:21,840 --> 00:04:26,759
those UEFI variables obviously some are

95
00:04:24,720 --> 00:04:29,100
interesting to security researchers

96
00:04:26,759 --> 00:04:31,080
probably all of them are interested to

97
00:04:29,100 --> 00:04:33,419
Firmware developers but there are other

98
00:04:31,080 --> 00:04:36,139
groups for example I think those those

99
00:04:33,419 --> 00:04:38,100
variables can be interest interested to

100
00:04:36,139 --> 00:04:40,259
hypervisor developers or Cloud

101
00:04:38,100 --> 00:04:43,620
infrastructure administrators because

102
00:04:40,259 --> 00:04:45,240
those may change the behavior and the

103
00:04:43,620 --> 00:04:48,259
good process of the operating system

104
00:04:45,240 --> 00:04:52,259
into virtual in Virtual machines

105
00:04:48,259 --> 00:04:56,460
also confidential VMS is a topic that

106
00:04:52,259 --> 00:04:59,940
recently gaining on importance and for

107
00:04:56,460 --> 00:05:02,820
for those special VMS we focus on

108
00:04:59,940 --> 00:05:04,040
confidentiality probably UEFI secure boot

109
00:05:02,820 --> 00:05:07,440
variables

110
00:05:04,040 --> 00:05:10,560
and other security related variables

111
00:05:07,440 --> 00:05:13,759
would be important to know and to to

112
00:05:10,560 --> 00:05:13,759
know how to use those

