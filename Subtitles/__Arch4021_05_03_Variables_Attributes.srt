1
00:00:00,359 --> 00:00:05,160
as we already know each UEFI variable

2
00:00:02,879 --> 00:00:07,560
consists of identifying information and

3
00:00:05,160 --> 00:00:10,080
attributes identifying information are

4
00:00:07,560 --> 00:00:12,179
vendor good and variable name those

5
00:00:10,080 --> 00:00:14,519
parameters are provided when using set

6
00:00:12,179 --> 00:00:17,160
variable and get variable UEFI runtime

7
00:00:14,519 --> 00:00:19,380
functions so vendor GUID is a is a good

8
00:00:17,160 --> 00:00:21,840
which provides unique namespace for

9
00:00:19,380 --> 00:00:25,260
various variable names in that way

10
00:00:21,840 --> 00:00:27,539
thanks to vendor GUID we can avoid

11
00:00:25,260 --> 00:00:30,000
collision between the same variable

12
00:00:27,539 --> 00:00:32,759
names which may be used in the system by

13
00:00:30,000 --> 00:00:35,040
various vendors so each vendor should

14
00:00:32,759 --> 00:00:38,520
use its own vendor GUID in and in that

15
00:00:35,040 --> 00:00:40,680
way vendor creates its own namespace for

16
00:00:38,520 --> 00:00:43,440
the variable names in that way there can

17
00:00:40,680 --> 00:00:45,120
be multiple variable names the same

18
00:00:43,440 --> 00:00:47,100
variable names but in different

19
00:00:45,120 --> 00:00:49,739
vendorgoid space there are some

20
00:00:47,100 --> 00:00:52,980
architectural defined variables which

21
00:00:49,739 --> 00:00:55,320
have special van durgoid in EDK2 source

22
00:00:52,980 --> 00:00:59,120
code we can find it as a fe Global

23
00:00:55,320 --> 00:01:01,680
variable GUID and we will discuss those

24
00:00:59,120 --> 00:01:03,840
architectural data defined variables

25
00:01:01,680 --> 00:01:06,060
later in the course but here we would

26
00:01:03,840 --> 00:01:09,360
like to introduce attributes which are

27
00:01:06,060 --> 00:01:11,400
responsible for indicating how the data

28
00:01:09,360 --> 00:01:13,740
variables should be stored and

29
00:01:11,400 --> 00:01:16,680
maintained by the system our attributes

30
00:01:13,740 --> 00:01:19,500
Define when variable can be accessed and

31
00:01:16,680 --> 00:01:21,540
if given variable is volatile or non-non

32
00:01:19,500 --> 00:01:25,200
volatile We Will We Will dive in all

33
00:01:21,540 --> 00:01:28,380
mention it here attributes Fe variable

34
00:01:25,200 --> 00:01:30,659
non-volatile boot service access runtime

35
00:01:28,380 --> 00:01:34,100
service access hardware error or record

36
00:01:30,659 --> 00:01:37,259
and so on some of those names are can be

37
00:01:34,100 --> 00:01:39,960
self-explanatory but some have more

38
00:01:37,259 --> 00:01:42,240
complex meaning and there are quite a

39
00:01:39,960 --> 00:01:44,579
lot of rules related how those can be

40
00:01:42,240 --> 00:01:48,780
used effectively it is worth to note

41
00:01:44,579 --> 00:01:51,840
that in EDK2 Source we can find also EFI

42
00:01:48,780 --> 00:01:53,939
variable read only and that attribute is

43
00:01:51,840 --> 00:01:56,159
is a non-starter non-standard attribute

44
00:01:53,939 --> 00:01:57,840
and is just kept there for backward

45
00:01:56,159 --> 00:01:59,759
compatibility it was used in the past

46
00:01:57,840 --> 00:02:01,680
but please note that all those

47
00:01:59,759 --> 00:02:06,540
attributes are implemented as a bit

48
00:02:01,680 --> 00:02:08,759
Fields so to get desired Behavior or

49
00:02:06,540 --> 00:02:11,760
desired capabilities properties from the

50
00:02:08,759 --> 00:02:15,840
variable we will combine those bit

51
00:02:11,760 --> 00:02:18,060
fields for a given variable and and in

52
00:02:15,840 --> 00:02:21,180
that way we will set some properties

53
00:02:18,060 --> 00:02:23,099
there are quite tricky rules described

54
00:02:21,180 --> 00:02:24,360
in UEFI Spec regarding the link with

55
00:02:23,099 --> 00:02:27,780
various we have five variables

56
00:02:24,360 --> 00:02:31,020
attributes for example if you set no

57
00:02:27,780 --> 00:02:33,900
attributes related to access this calls

58
00:02:31,020 --> 00:02:36,060
a set variable to delete our variable if

59
00:02:33,900 --> 00:02:37,680
you don't know that behavior then you

60
00:02:36,060 --> 00:02:40,080
might be surprised with the result maybe

61
00:02:37,680 --> 00:02:42,660
all of that is not that important at

62
00:02:40,080 --> 00:02:44,760
this point but it is good to know that

63
00:02:42,660 --> 00:02:47,640
if something unexpected happens tricky

64
00:02:44,760 --> 00:02:49,319
rules May might be the reason and we

65
00:02:47,640 --> 00:02:51,420
should check the spec for the detailed

66
00:02:49,319 --> 00:02:54,420
Behavior reading and understanding

67
00:02:51,420 --> 00:02:56,580
attribute rules how those apply and and

68
00:02:54,420 --> 00:02:59,700
understanding qfi species with that

69
00:02:56,580 --> 00:03:01,980
regard is quite fascinating exercise it

70
00:02:59,700 --> 00:03:04,560
seems that spec writers complicated

71
00:03:01,980 --> 00:03:06,720
overall analysis and implementation so

72
00:03:04,560 --> 00:03:10,260
for example the Fe variable hardware

73
00:03:06,720 --> 00:03:12,959
error record like attribute related

74
00:03:10,260 --> 00:03:15,420
information cannot be found unless we

75
00:03:12,959 --> 00:03:18,540
know that in other places in in

76
00:03:15,420 --> 00:03:21,840
specification our authors refer to it

77
00:03:18,540 --> 00:03:24,599
using abbreviation HR also description

78
00:03:21,840 --> 00:03:26,580
of the way attributes work is

79
00:03:24,599 --> 00:03:29,099
interleaved in specification wall of

80
00:03:26,580 --> 00:03:32,700
text in in kind of description prose

81
00:03:29,099 --> 00:03:35,220
form instead of using table like or like

82
00:03:32,700 --> 00:03:37,379
relation diagram with clear name of the

83
00:03:35,220 --> 00:03:40,680
attribute and clear the description of

84
00:03:37,379 --> 00:03:42,659
behavior of course it is easier to judge

85
00:03:40,680 --> 00:03:45,060
than to write good spec but let's hope

86
00:03:42,659 --> 00:03:47,879
future version of the specification will

87
00:03:45,060 --> 00:03:51,080
improve usability with that regard

88
00:03:47,879 --> 00:03:55,440
because of those values weird

89
00:03:51,080 --> 00:03:57,480
relations and and tricky rules I think

90
00:03:55,440 --> 00:03:59,940
security researchers may find a lot of

91
00:03:57,480 --> 00:04:03,420
interesting mistakes and Corner cases in

92
00:03:59,940 --> 00:04:06,780
UEFI variables usage but let's dive into

93
00:04:03,420 --> 00:04:10,200
the attributes itself and start with

94
00:04:06,780 --> 00:04:12,980
explaining a EFI variable non-volatile

95
00:04:10,200 --> 00:04:17,400
so this sounds like a self-explanatory

96
00:04:12,980 --> 00:04:19,919
if available got this attribute it is

97
00:04:17,400 --> 00:04:22,919
treated as a non-volatile it started in

98
00:04:19,919 --> 00:04:26,220
non-volatile memory instead of RAM and

99
00:04:22,919 --> 00:04:28,199
it will be preserved across reboots and

100
00:04:26,220 --> 00:04:31,080
as we explained earlier non-volatized

101
00:04:28,199 --> 00:04:32,820
storage may be limited so so use of

102
00:04:31,080 --> 00:04:36,540
those variables should be only for

103
00:04:32,820 --> 00:04:39,120
reasonable purpose next variable is EFI

104
00:04:36,540 --> 00:04:41,460
variable runtime access and partially

105
00:04:39,120 --> 00:04:43,979
description also applies to boot server

106
00:04:41,460 --> 00:04:46,259
boot service access runtime access means

107
00:04:43,979 --> 00:04:49,620
a variable can be accessed by runtime

108
00:04:46,259 --> 00:04:52,560
Services what means this access this is

109
00:04:49,620 --> 00:04:55,220
accessed from operating system level and

110
00:04:52,560 --> 00:04:58,259
to be precise this is access after

111
00:04:55,220 --> 00:05:01,620
bootloader call it exit boot Services

112
00:04:58,259 --> 00:05:04,020
function which which happens at the

113
00:05:01,620 --> 00:05:06,540
handle from VDS to the operating system

114
00:05:04,020 --> 00:05:09,240
so setting runtime access attributes

115
00:05:06,540 --> 00:05:12,419
implies also setting boot Services

116
00:05:09,240 --> 00:05:15,060
access I attribute but the

117
00:05:12,419 --> 00:05:17,940
responsibility of doing that is

118
00:05:15,060 --> 00:05:18,960
delegated to the set variable function

119
00:05:17,940 --> 00:05:21,720
caller

120
00:05:18,960 --> 00:05:24,660
and variables which do not have that

121
00:05:21,720 --> 00:05:26,820
attribute are not visible from the by

122
00:05:24,660 --> 00:05:28,620
the get variables runtime Services

123
00:05:26,820 --> 00:05:31,500
function when it is called from

124
00:05:28,620 --> 00:05:34,020
operating system so it's like if we not

125
00:05:31,500 --> 00:05:36,960
set this this runtime access we cannot

126
00:05:34,020 --> 00:05:39,479
see in operating system uh those those

127
00:05:36,960 --> 00:05:42,080
variables and and we will just get from

128
00:05:39,479 --> 00:05:45,180
get variables every note not found error

129
00:05:42,080 --> 00:05:48,080
we don't explain both service access

130
00:05:45,180 --> 00:05:51,600
attribute because it seems to be

131
00:05:48,080 --> 00:05:53,639
self-explanatory it simply means that if

132
00:05:51,600 --> 00:05:56,400
a variable got put service access but

133
00:05:53,639 --> 00:05:58,380
don't have runtime access it means that

134
00:05:56,400 --> 00:06:01,500
variable is only accessible accessible

135
00:05:58,380 --> 00:06:04,320
before exit boot Services function was

136
00:06:01,500 --> 00:06:07,560
called Next attribute is Fe variable

137
00:06:04,320 --> 00:06:10,560
hardware error record so this variable

138
00:06:07,560 --> 00:06:13,860
use a very specific vendor go it was

139
00:06:10,560 --> 00:06:15,600
defined in the UEFI spec and meaning of

140
00:06:13,860 --> 00:06:18,479
the content of the variables is

141
00:06:15,600 --> 00:06:21,419
architecturally defined those variables

142
00:06:18,479 --> 00:06:24,419
are typically started in NV Ram Aria

143
00:06:21,419 --> 00:06:26,880
dedicated for recording errors more

144
00:06:24,419 --> 00:06:29,940
information about that about that

145
00:06:26,880 --> 00:06:33,600
hardware error record is in appendix P

146
00:06:29,940 --> 00:06:35,940
of UEFI specification and it just

147
00:06:33,600 --> 00:06:38,520
describe how hardware error record

148
00:06:35,940 --> 00:06:39,479
persistence can be used by operating

149
00:06:38,520 --> 00:06:41,460
system

150
00:06:39,479 --> 00:06:45,300
of course because those information

151
00:06:41,460 --> 00:06:49,080
stored in in this invariable using this

152
00:06:45,300 --> 00:06:50,639
attribute are implementation specific so

153
00:06:49,080 --> 00:06:53,460
to understand that content you will

154
00:06:50,639 --> 00:06:57,000
probably need a specification from your

155
00:06:53,460 --> 00:06:59,280
BIOS vendor or maybe your operating

156
00:06:57,000 --> 00:07:02,100
system vendor may have access to that

157
00:06:59,280 --> 00:07:04,280
specification and in that way BIOS

158
00:07:02,100 --> 00:07:08,100
vendor and operating system vendor can

159
00:07:04,280 --> 00:07:11,039
use those use those variables for some

160
00:07:08,100 --> 00:07:13,699
useful purpose the last the variable is

161
00:07:11,039 --> 00:07:16,620
Fe variable authenticated right access

162
00:07:13,699 --> 00:07:19,319
this variable is already deprecated but

163
00:07:16,620 --> 00:07:22,139
it's purpose was to authenticate set

164
00:07:19,319 --> 00:07:24,919
variable color when when creating new

165
00:07:22,139 --> 00:07:27,300
variables and when updating variables

166
00:07:24,919 --> 00:07:29,520
there are newer attributes which are

167
00:07:27,300 --> 00:07:33,060
used for that and we will discuss those

168
00:07:29,520 --> 00:07:34,620
in more details during the this course

169
00:07:33,060 --> 00:07:37,620
on next slides

170
00:07:34,620 --> 00:07:40,080
let's start with EFI variable time based

171
00:07:37,620 --> 00:07:42,780
authenticated right access that

172
00:07:40,080 --> 00:07:45,319
attribute is set when calling set

173
00:07:42,780 --> 00:07:49,440
variable function the authentication

174
00:07:45,319 --> 00:07:52,800
solutes special descriptor call it EFI

175
00:07:49,440 --> 00:07:56,039
variable Authentication 2. uh what what

176
00:07:52,800 --> 00:07:57,599
means uh that setting this time-based

177
00:07:56,039 --> 00:08:00,479
authenticated right access attribute

178
00:07:57,599 --> 00:08:03,479
just requires some additional structures

179
00:08:00,479 --> 00:08:07,860
some special structure to be filled and

180
00:08:03,479 --> 00:08:11,660
and provided to set variables so so the

181
00:08:07,860 --> 00:08:11,660
set variable call can succeed

182
00:08:11,699 --> 00:08:17,400
the value of special variables which use

183
00:08:14,400 --> 00:08:19,800
this attribute which is a time-based

184
00:08:17,400 --> 00:08:22,379
authenticated right right access for

185
00:08:19,800 --> 00:08:25,860
example there is always recovery family

186
00:08:22,379 --> 00:08:27,720
of variables used for for example for

187
00:08:25,860 --> 00:08:30,720
warranty service reconfiguration

188
00:08:27,720 --> 00:08:33,779
Diagnostics and other special operating

189
00:08:30,720 --> 00:08:36,959
system recovery behaviors uh there are

190
00:08:33,779 --> 00:08:38,940
also secure boot policy variables which

191
00:08:36,959 --> 00:08:41,339
must be created with time-based

192
00:08:38,940 --> 00:08:45,300
authenticated right access attribute set

193
00:08:41,339 --> 00:08:47,279
and those secure related variables and

194
00:08:45,300 --> 00:08:50,100
unfortunately are out of scope of this

195
00:08:47,279 --> 00:08:53,820
lecture but we will discuss it in deep

196
00:08:50,100 --> 00:08:57,240
in other OST courses

197
00:08:53,820 --> 00:08:59,100
we will discuss use of authentication to

198
00:08:57,240 --> 00:09:01,980
descriptors of the special structure

199
00:08:59,100 --> 00:09:06,620
which is provided to set variable

200
00:09:01,980 --> 00:09:09,959
um later in in this section next when

201
00:09:06,620 --> 00:09:13,500
EFI variable up and right attribute is

202
00:09:09,959 --> 00:09:15,600
set while calling set variable then any

203
00:09:13,500 --> 00:09:18,899
provided data will be opened to the

204
00:09:15,600 --> 00:09:22,019
existing variable and unfortunately when

205
00:09:18,899 --> 00:09:24,120
we when we calling get variables and we

206
00:09:22,019 --> 00:09:26,160
look at attributes this app and write

207
00:09:24,120 --> 00:09:28,800
will not be visible there because this

208
00:09:26,160 --> 00:09:31,980
is like just the attribute which is

209
00:09:28,800 --> 00:09:34,260
provided for a set variable to extend

210
00:09:31,980 --> 00:09:36,060
given variable use of the attribute has

211
00:09:34,260 --> 00:09:38,100
a very interesting Corner cases for

212
00:09:36,060 --> 00:09:42,120
example if nor a label time source is

213
00:09:38,100 --> 00:09:44,519
available for providing timestamp while

214
00:09:42,120 --> 00:09:45,660
using authentication to descriptor we

215
00:09:44,519 --> 00:09:48,240
will talk about content of

216
00:09:45,660 --> 00:09:49,920
authentication to descriptor later but

217
00:09:48,240 --> 00:09:53,339
right now we have to know that it

218
00:09:49,920 --> 00:09:55,680
contains a timestamp inside so if there

219
00:09:53,339 --> 00:09:58,080
is a plug phone which has no ability to

220
00:09:55,680 --> 00:09:59,600
provide this reliable time source for

221
00:09:58,080 --> 00:10:03,060
that timestamp

222
00:09:59,600 --> 00:10:05,700
implementation may decide to add a add

223
00:10:03,060 --> 00:10:09,959
variable despite the timestamp is not

224
00:10:05,700 --> 00:10:13,080
there when opened right attribute is set

225
00:10:09,959 --> 00:10:16,019
so this is like a rare case very special

226
00:10:13,080 --> 00:10:19,260
case and in this situation when the time

227
00:10:16,019 --> 00:10:21,600
style will be set to all zeros it

228
00:10:19,260 --> 00:10:23,940
essentially is ignored because we have

229
00:10:21,600 --> 00:10:25,980
up and right attribute

230
00:10:23,940 --> 00:10:27,959
so this is like a quarter case this guy

231
00:10:25,980 --> 00:10:30,420
described in the specification up and

232
00:10:27,959 --> 00:10:32,760
right are very useful because they can

233
00:10:30,420 --> 00:10:35,820
create data databases which for example

234
00:10:32,760 --> 00:10:38,279
are used in UEFI Secure boot to store a

235
00:10:35,820 --> 00:10:41,220
load and the nine signatures and hashes

236
00:10:38,279 --> 00:10:44,160
use of such variable can be leveraged

237
00:10:41,220 --> 00:10:46,980
also by already discussed Hardware

238
00:10:44,160 --> 00:10:49,320
hardware error record variables to

239
00:10:46,980 --> 00:10:50,760
record multiple error entries in in one

240
00:10:49,320 --> 00:10:55,680
variable

241
00:10:50,760 --> 00:10:59,100
and finally the EFI variable enhanced

242
00:10:55,680 --> 00:11:00,779
authenticated access attribute this

243
00:10:59,100 --> 00:11:03,060
attribute is very similar to already

244
00:11:00,779 --> 00:11:05,940
discussed time-based authenticated right

245
00:11:03,060 --> 00:11:09,120
access but use different descriptor use

246
00:11:05,940 --> 00:11:11,399
descriptor call it authentication 3 and

247
00:11:09,120 --> 00:11:13,140
this handing this descriptor is way way

248
00:11:11,399 --> 00:11:15,480
more complex than handling

249
00:11:13,140 --> 00:11:17,220
authentication too time-based

250
00:11:15,480 --> 00:11:19,260
authenticated right access attribute and

251
00:11:17,220 --> 00:11:22,140
enhance authenticated access attribute

252
00:11:19,260 --> 00:11:24,620
are mutually exclusive because those use

253
00:11:22,140 --> 00:11:28,160
different descriptors and like this

254
00:11:24,620 --> 00:11:30,660
simply this cannot be handled together

255
00:11:28,160 --> 00:11:34,320
variables with enhanced authenticated

256
00:11:30,660 --> 00:11:36,779
access attribute return when calling get

257
00:11:34,320 --> 00:11:39,000
variables so that will not only variable

258
00:11:36,779 --> 00:11:41,459
data but also metadata associated with

259
00:11:39,000 --> 00:11:44,420
that and in that metadata we typically

260
00:11:41,459 --> 00:11:47,160
get information about the certificate

261
00:11:44,420 --> 00:11:50,100
associated with the variable and we will

262
00:11:47,160 --> 00:11:53,579
talk about that later when analyzing the

263
00:11:50,100 --> 00:11:55,260
Authentication 3 descriptor use so as I

264
00:11:53,579 --> 00:11:57,779
said like authentication enhances

265
00:11:55,260 --> 00:12:01,860
authenticated access attribute is is

266
00:11:57,779 --> 00:12:04,860
most complex to handle and and this is

267
00:12:01,860 --> 00:12:08,120
most complex this this Define most

268
00:12:04,860 --> 00:12:11,160
complex complex UEFI variable of all

269
00:12:08,120 --> 00:12:14,160
variables defined in UEFI Spec when we

270
00:12:11,160 --> 00:12:16,920
analyzed a source code of EDK2 we also

271
00:12:14,160 --> 00:12:19,740
realized that there is no sign of use of

272
00:12:16,920 --> 00:12:22,620
enhanced authenticated access variables

273
00:12:19,740 --> 00:12:26,279
so that probably means this is used by

274
00:12:22,620 --> 00:12:29,459
some proprietary implementations

275
00:12:26,279 --> 00:12:31,380
let's take a look at every variable

276
00:12:29,459 --> 00:12:34,800
authentication structures

277
00:12:31,380 --> 00:12:39,959
so first authentication to we can see

278
00:12:34,800 --> 00:12:44,040
that it contains every time timestamp

279
00:12:39,959 --> 00:12:45,600
and win certificate you if I call it out

280
00:12:44,040 --> 00:12:49,380
info

281
00:12:45,600 --> 00:12:53,160
and the timestamp is just just time

282
00:12:49,380 --> 00:12:57,660
structure every time structure which is

283
00:12:53,160 --> 00:13:00,420
expressed in GMT time zone and out info

284
00:12:57,660 --> 00:13:02,959
is a structure containing header

285
00:13:00,420 --> 00:13:07,880
certificate type and certificate data

286
00:13:02,959 --> 00:13:11,399
and in case of of our authentication to

287
00:13:07,880 --> 00:13:14,579
descriptor only pkcs7 certificates are

288
00:13:11,399 --> 00:13:19,320
are supported a more complex structure

289
00:13:14,579 --> 00:13:19,980
is authentication 3 which we have here

290
00:13:19,320 --> 00:13:23,060
um

291
00:13:19,980 --> 00:13:25,500
it contains a

292
00:13:23,060 --> 00:13:29,040
various various information including

293
00:13:25,500 --> 00:13:32,180
version type metadata size and flux so

294
00:13:29,040 --> 00:13:36,740
first of all version seems seem to be

295
00:13:32,180 --> 00:13:39,660
self-explanatory but but as we can see

296
00:13:36,740 --> 00:13:41,880
it was omitted in the authentication

297
00:13:39,660 --> 00:13:45,060
tool and that's probably why we see

298
00:13:41,880 --> 00:13:47,160
authentication 3 descriptor it just

299
00:13:45,060 --> 00:13:48,620
proves that this field should be here

300
00:13:47,160 --> 00:13:53,779
from the beginning

301
00:13:48,620 --> 00:13:56,820
then we have type and type triggers

302
00:13:53,779 --> 00:13:59,480
triggers adding secondary secondary

303
00:13:56,820 --> 00:14:02,540
descriptor right after the

304
00:13:59,480 --> 00:14:07,200
authentication tree descriptor

305
00:14:02,540 --> 00:14:11,220
and there are two potential types which

306
00:14:07,200 --> 00:14:13,560
are defined above first is Fe variable

307
00:14:11,220 --> 00:14:14,600
authentication three times times time

308
00:14:13,560 --> 00:14:19,680
type

309
00:14:14,600 --> 00:14:22,740
descriptor and second is non-stype nonce

310
00:14:19,680 --> 00:14:25,860
type the descriptor uh we will discuss

311
00:14:22,740 --> 00:14:29,820
how those are used when and when we will

312
00:14:25,860 --> 00:14:33,660
analyze authentication tree usage flow

313
00:14:29,820 --> 00:14:36,839
um then we have metadata size meta data

314
00:14:33,660 --> 00:14:40,380
size is everything is the size of

315
00:14:36,839 --> 00:14:42,240
everything except variables data and

316
00:14:40,380 --> 00:14:42,980
then we have

317
00:14:42,240 --> 00:14:46,519
um

318
00:14:42,980 --> 00:14:49,699
flux and those flags

319
00:14:46,519 --> 00:14:54,420
can cause additional verification steps

320
00:14:49,699 --> 00:14:58,620
luckily luckily UEFI specification right

321
00:14:54,420 --> 00:15:02,360
now Define only one flag which is a EFI

322
00:14:58,620 --> 00:15:07,500
variable enhanced out flag update set

323
00:15:02,360 --> 00:15:12,139
which when indicated it it it says there

324
00:15:07,500 --> 00:15:14,940
is there is a new certificate present

325
00:15:12,139 --> 00:15:17,480
the structure of new certificate present

326
00:15:14,940 --> 00:15:20,100
right after

327
00:15:17,480 --> 00:15:23,040
authentication 3 descriptor

328
00:15:20,100 --> 00:15:26,579
and this new certificate would be said

329
00:15:23,040 --> 00:15:30,060
as a Authority for for for the for for

330
00:15:26,579 --> 00:15:32,279
the variable to update the variable uh

331
00:15:30,060 --> 00:15:35,579
of course uh this cannot happen

332
00:15:32,279 --> 00:15:38,880
arbitrary and in such case there is Need

333
00:15:35,579 --> 00:15:42,180
for one more structure which will

334
00:15:38,880 --> 00:15:45,660
contain cyanide Sinead data which will

335
00:15:42,180 --> 00:15:49,320
authorize update of that certificate and

336
00:15:45,660 --> 00:15:50,180
that's uh and that's EFI variable

337
00:15:49,320 --> 00:15:54,860
um

338
00:15:50,180 --> 00:15:54,860
uh authentication 3 descriptor

