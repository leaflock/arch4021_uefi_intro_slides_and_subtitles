1
00:00:00,240 --> 00:00:04,960
So what's the boot firmware history.

2
00:00:03,520 --> 00:00:06,720
By the way the

3
00:00:04,960 --> 00:00:07,759
the picture here

4
00:00:06,720 --> 00:00:08,639
present

5
00:00:07,759 --> 00:00:10,960
the

6
00:00:08,639 --> 00:00:14,400
PDP 8.

7
00:00:10,960 --> 00:00:16,400
This is like very old computer, but

8
00:00:14,400 --> 00:00:18,080
this picture is very

9
00:00:16,400 --> 00:00:22,160
interesting because

10
00:00:18,080 --> 00:00:23,600
there are some switches to bootstrap

11
00:00:22,160 --> 00:00:27,119


12
00:00:23,600 --> 00:00:29,599
the computer. 

13
00:00:27,119 --> 00:00:31,119
So, in the very early stage the form of BIOS was

14
00:00:29,599 --> 00:00:32,880
just

15
00:00:31,119 --> 00:00:34,800
concrete set of

16
00:00:32,880 --> 00:00:36,399
registers which could

17
00:00:34,800 --> 00:00:37,920
happen by

18
00:00:36,399 --> 00:00:40,000
toggling some

19
00:00:37,920 --> 00:00:42,000
switches.

20
00:00:40,000 --> 00:00:43,760
And by the way uh there is very

21
00:00:42,000 --> 00:00:45,360
interesting

22
00:00:43,760 --> 00:00:47,039
wikipedia article

23
00:00:45,360 --> 00:00:50,160
about booting,

24
00:00:47,039 --> 00:00:53,199
I really recommend that it's very

25
00:00:50,160 --> 00:00:54,480
rich and very

26
00:00:53,199 --> 00:00:57,520
extensive.

27
00:00:54,480 --> 00:00:59,120
So, it's a lot to learn how computers

28
00:00:57,520 --> 00:01:01,680
evolve in

29
00:00:59,120 --> 00:01:04,479
bootstrapping process, booting sequence.

30
00:01:01,680 --> 00:01:07,200
So, during early days computer booting

31
00:01:04,479 --> 00:01:08,400
system was just entered manually by the

32
00:01:07,200 --> 00:01:11,520
operator

33
00:01:08,400 --> 00:01:14,240
and of course from that point it

34
00:01:11,520 --> 00:01:17,520
goes further to be more and more

35
00:01:14,240 --> 00:01:19,439
sophisticated. But old computers were

36
00:01:17,520 --> 00:01:22,080
way way less sophisticated and

37
00:01:19,439 --> 00:01:24,720
initialization mechanism

38
00:01:22,080 --> 00:01:26,479
were much simpler. There were no not a

39
00:01:24,720 --> 00:01:27,439
lot of peripherals so interaction with

40
00:01:26,479 --> 00:01:29,600
them

41
00:01:27,439 --> 00:01:31,280
could be hard coded. But,

42
00:01:29,600 --> 00:01:33,040
after some time the

43
00:01:31,280 --> 00:01:35,520
computing system were more and more

44
00:01:33,040 --> 00:01:38,400
sophisticated.

45
00:01:35,520 --> 00:01:43,119
So, these days we have like this

46
00:01:38,400 --> 00:01:46,000
cockpits of very complex avionic

47
00:01:43,119 --> 00:01:48,560
computers embedded systems and those

48
00:01:46,000 --> 00:01:51,280
also have to put somehow and also have

49
00:01:48,560 --> 00:01:53,840
to initialize the hardware

50
00:01:51,280 --> 00:01:56,240
underlying.

51
00:01:53,840 --> 00:01:58,479
And because we have

52
00:01:56,240 --> 00:02:02,000
some chips which can be used in very

53
00:01:58,479 --> 00:02:04,240
different applications and because

54
00:02:02,000 --> 00:02:05,680
we have extensive configurability of

55
00:02:04,240 --> 00:02:08,640
these chips,

56
00:02:05,680 --> 00:02:11,520
we have to have sophisticated software

57
00:02:08,640 --> 00:02:13,360
or firmware, a close to hardware software

58
00:02:11,520 --> 00:02:14,480
which will initialize

59
00:02:13,360 --> 00:02:16,800


60
00:02:14,480 --> 00:02:18,959
this hardware to be used in given

61
00:02:16,800 --> 00:02:20,959
application. So thanks to that, thanks to

62
00:02:18,959 --> 00:02:24,239
firmware we have more generic hardware

63
00:02:20,959 --> 00:02:25,440
which can be used in multiple

64
00:02:24,239 --> 00:02:27,520
applications.

65
00:02:25,440 --> 00:02:29,360
So, in this case

66
00:02:27,520 --> 00:02:30,239
by firmware we mean

67
00:02:29,360 --> 00:02:32,560


68
00:02:30,239 --> 00:02:34,480
BIOS, basic input output system which

69
00:02:32,560 --> 00:02:36,879
history I will describe on further

70
00:02:34,480 --> 00:02:40,000
slides.

71
00:02:36,879 --> 00:02:41,950
But, essentially the goal of this

72
00:02:40,000 --> 00:02:43,680
firmware initial firmware which

73
00:02:41,950 --> 00:02:46,560
.

74
00:02:43,680 --> 00:02:49,440
configures our hardware is to

75
00:02:46,560 --> 00:02:52,160
initialize test the hardware make sure

76
00:02:49,440 --> 00:02:54,000
that we can load the operating system or

77
00:02:52,160 --> 00:02:55,280
target application which can be

78
00:02:54,000 --> 00:02:58,879
for example

79
00:02:55,280 --> 00:03:01,920
RTOS or some Bare Metal application

80
00:02:58,879 --> 00:03:03,680
then initialize and provide

81
00:03:01,920 --> 00:03:05,519
system management function

82
00:03:03,680 --> 00:03:07,519
which may mean some power management,

83
00:03:05,519 --> 00:03:10,400
some thermal management, or some

84
00:03:07,519 --> 00:03:11,920
administrative related stuff. And then

85
00:03:10,400 --> 00:03:14,879
potentially

86
00:03:11,920 --> 00:03:17,040
in most modern system load patches

87
00:03:14,879 --> 00:03:19,200
during the boot process. For example

88
00:03:17,040 --> 00:03:21,200
do the firmware update or do the

89
00:03:19,200 --> 00:03:24,319
microcode patching

90
00:03:21,200 --> 00:03:24,319
while platform boot.




