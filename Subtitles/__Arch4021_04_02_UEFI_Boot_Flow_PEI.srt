1
00:00:00,480 --> 00:00:04,319
So, next phase is Pre-EFI-Initialization (PEI)

2
00:00:03,520 --> 00:00:06,799
phase.

3
00:00:04,319 --> 00:00:09,040
And this phase contain

4
00:00:06,799 --> 00:00:11,840
steps like processor

5
00:00:09,040 --> 00:00:14,160
initialization, chipset initialization

6
00:00:11,840 --> 00:00:15,519
and mainboard specific

7
00:00:14,160 --> 00:00:17,440
initialization.

8
00:00:15,519 --> 00:00:19,039
During this phase

9
00:00:17,440 --> 00:00:21,359
typically there is memory initialization

10
00:00:19,039 --> 00:00:23,439
but we will discuss that maybe in

11
00:00:21,359 --> 00:00:26,000
detail on the next slide. What is very

12
00:00:23,439 --> 00:00:28,080
important this phase typically contain

13
00:00:26,000 --> 00:00:30,960
proprietary

14
00:00:28,080 --> 00:00:32,320
IP-protected

15
00:00:30,960 --> 00:00:35,200
stuff that

16
00:00:32,320 --> 00:00:37,680
most of the Silicon vendors consider

17
00:00:35,200 --> 00:00:37,680
secret.

18
00:00:38,160 --> 00:00:42,399
So,

19
00:00:39,680 --> 00:00:44,559
we can say that PEI is the main subject

20
00:00:42,399 --> 00:00:47,200
of a platform initialization

21
00:00:44,559 --> 00:00:49,280
specification and the specification

22
00:00:47,200 --> 00:00:51,520
defines goals of that phase as a

23
00:00:49,280 --> 00:00:54,640
maintaining chain of trust consistent

24
00:00:51,520 --> 00:00:57,600
so when we know that SEC

25
00:00:54,640 --> 00:01:00,559
initiated the chain of trust

26
00:00:57,600 --> 00:01:02,719
as a root of trust component and then

27
00:01:00,559 --> 00:01:05,920
PEI should maintain that

28
00:01:02,719 --> 00:01:08,720
security capabilities

29
00:01:05,920 --> 00:01:10,560
it should perform early initialization,

30
00:01:08,720 --> 00:01:13,040
memory initialization

31
00:01:10,560 --> 00:01:15,759
and it should prepare hand-off blocks

32
00:01:13,040 --> 00:01:19,360
for DXE phase and then when passing

33
00:01:15,759 --> 00:01:20,720
control to DXE also pass those data

34
00:01:19,360 --> 00:01:22,960
in HOBs.

35
00:01:20,720 --> 00:01:22,960


36
00:01:23,119 --> 00:01:27,280
Its structure

37
00:01:25,119 --> 00:01:30,320
is very similar to DXE

38
00:01:27,280 --> 00:01:32,720
like but the naming is different

39
00:01:30,320 --> 00:01:34,240
there is limitation in the memory

40
00:01:32,720 --> 00:01:36,799
and

41
00:01:34,240 --> 00:01:39,759
capabilities in terms of

42
00:01:36,799 --> 00:01:42,479
PEI modules versus DXE drivers is

43
00:01:39,759 --> 00:01:45,920
very different because we

44
00:01:42,479 --> 00:01:48,079
are still yet in limited environment.

45
00:01:45,920 --> 00:01:50,640
And 

46
00:01:48,079 --> 00:01:53,680
what's important to understand

47
00:01:50,640 --> 00:01:56,159
while talking about modern computer

48
00:01:53,680 --> 00:01:58,399
initialization and this was mentioned in

49
00:01:56,159 --> 00:02:00,960
other lectures is that memory

50
00:01:58,399 --> 00:02:03,439
initialization is one of the most

51
00:02:00,960 --> 00:02:04,960
complex parts of the boot process.

52
00:02:03,439 --> 00:02:07,040
And

53
00:02:04,960 --> 00:02:09,759
why is that? So, there are many reasons

54
00:02:07,040 --> 00:02:13,280
for that

55
00:02:09,759 --> 00:02:16,480
one of the reasons is that

56
00:02:13,280 --> 00:02:17,840
memory components getting old and

57
00:02:16,480 --> 00:02:19,840
they may

58
00:02:17,840 --> 00:02:21,520
change capabilities over time so that

59
00:02:19,840 --> 00:02:26,080
means there is need for

60
00:02:21,520 --> 00:02:29,200
retraining memory to obtain a

61
00:02:26,080 --> 00:02:31,760
maximum performance, maximum speed on

62
00:02:29,200 --> 00:02:32,800
the buses that's one thing. The other

63
00:02:31,760 --> 00:02:34,560
thing is

64
00:02:32,800 --> 00:02:36,560
every design is little bit different

65
00:02:34,560 --> 00:02:37,920
every hardware design design is a little

66
00:02:36,560 --> 00:02:40,000
bit different and

67
00:02:37,920 --> 00:02:43,040
BIOS have to boot with various devices

68
00:02:40,000 --> 00:02:45,760
in various

69
00:02:43,040 --> 00:02:47,440
designs also the training the memory

70
00:02:45,760 --> 00:02:50,720
controller have to deal with various

71
00:02:47,440 --> 00:02:52,640
memory configuration which vary a lot

72
00:02:50,720 --> 00:02:55,200
and because of that, there is need for

73
00:02:52,640 --> 00:02:57,760
some sophisticated algorithms

74
00:02:55,200 --> 00:03:01,120
to establish correct signaling and

75
00:02:57,760 --> 00:03:03,519
correct timing for

76
00:03:01,120 --> 00:03:06,720
memory use.

77
00:03:03,519 --> 00:03:09,440
Typically this initialization of a

78
00:03:06,720 --> 00:03:13,760
model is provided in binary component

79
00:03:09,440 --> 00:03:13,760
delivered by silicon vendor

80
00:03:13,840 --> 00:03:17,599
and

81
00:03:14,640 --> 00:03:19,280
sometimes even in modern

82
00:03:17,599 --> 00:03:21,840
platforms

83
00:03:19,280 --> 00:03:24,480
this action of memory initialization is

84
00:03:21,840 --> 00:03:28,080
offloaded to dedicated processor, which

85
00:03:24,480 --> 00:03:29,840
is closed source peripheral processor

86
00:03:28,080 --> 00:03:32,560
which 

87
00:03:29,840 --> 00:03:34,159
may have more goals than just

88
00:03:32,560 --> 00:03:37,440
memory initialization, it may

89
00:03:34,159 --> 00:03:40,720
also serve

90
00:03:37,440 --> 00:03:40,720
some security functions.

91
00:03:41,040 --> 00:03:45,680


92
00:03:42,000 --> 00:03:46,640
So, silicon vendors consider this

93
00:03:45,680 --> 00:03:49,440
code

94
00:03:46,640 --> 00:03:52,640
very expensive and and complex and this

95
00:03:49,440 --> 00:03:56,239
is because one new memory initialization

96
00:03:52,640 --> 00:03:58,080
specification coming out like bodies

97
00:03:56,239 --> 00:04:00,879
like JEDEC

98
00:03:58,080 --> 00:04:02,560
and companies which produce memory

99
00:04:00,879 --> 00:04:05,040


100
00:04:02,560 --> 00:04:07,280
modules discuss with silicon

101
00:04:05,040 --> 00:04:10,159
vendors how this will be initialized

102
00:04:07,280 --> 00:04:12,640
what's the best way and

103
00:04:10,159 --> 00:04:15,920
they of course spent a lot of money on

104
00:04:12,640 --> 00:04:19,120
on architects that establish

105
00:04:15,920 --> 00:04:21,120
those protocols and those algorithms.

106
00:04:19,120 --> 00:04:22,800
We may ask ourselves why we even need

107
00:04:21,120 --> 00:04:24,080
memory initialization?

108
00:04:22,800 --> 00:04:26,320


109
00:04:24,080 --> 00:04:28,800
So, we already said that this module

110
00:04:26,320 --> 00:04:31,040
is getting old and the designs are

111
00:04:28,800 --> 00:04:33,680
different, but a key point is to achieve

112
00:04:31,040 --> 00:04:36,080
maximum bitrate and performance numbers

113
00:04:33,680 --> 00:04:37,759
in the

114
00:04:36,080 --> 00:04:41,520
communication with the memory so

115
00:04:37,759 --> 00:04:43,840
so we can squeeze from our hardware 

116
00:04:41,520 --> 00:04:43,840
maximum.

117
00:04:44,080 --> 00:04:49,040
How PEI structure look like?

118
00:04:46,800 --> 00:04:50,160
So, PEI consists of the following

119
00:04:49,040 --> 00:04:51,280
elements.

120
00:04:50,160 --> 00:04:53,360


121
00:04:51,280 --> 00:04:56,000
First of all there was already discussed

122
00:04:53,360 --> 00:04:57,759
a PEI Foundation, it's just like

123
00:04:56,000 --> 00:04:58,639
general

124
00:04:57,759 --> 00:04:59,919
code

125
00:04:58,639 --> 00:05:02,080
wrapping the

126
00:04:59,919 --> 00:05:04,479
the phase and kind of scheduling the

127
00:05:02,080 --> 00:05:08,080
phase, scheduling what is executed in that

128
00:05:04,479 --> 00:05:09,680
phase. It manages PEI execution and

129
00:05:08,080 --> 00:05:10,800
transition to the next phase which is

130
00:05:09,680 --> 00:05:13,840
DXE.

131
00:05:10,800 --> 00:05:16,400
It managed

132
00:05:13,840 --> 00:05:19,520
loading the PEI modules which are next

133
00:05:16,400 --> 00:05:21,560
component PEIMs

134
00:05:19,520 --> 00:05:23,840
and those are single proposed

135
00:05:21,560 --> 00:05:26,560
interchangeable pieces of initialization

136
00:05:23,840 --> 00:05:27,600
code. We can say that those are

137
00:05:26,560 --> 00:05:30,080
very

138
00:05:27,600 --> 00:05:31,360
early stage

139
00:05:30,080 --> 00:05:35,039
drivers.

140
00:05:31,360 --> 00:05:36,320
There are also things like

141
00:05:35,039 --> 00:05:39,440
PEIM-to-PEIM interfaces,

142
00:05:36,320 --> 00:05:41,600
in short PPIs, they provide communication

143
00:05:39,440 --> 00:05:45,440
between modules. To some extent we can

144
00:05:41,600 --> 00:05:48,800
say that PPIs are like protocols or

145
00:05:45,440 --> 00:05:52,320
like those are like protocols

146
00:05:48,800 --> 00:05:54,479
for discussion between modules.

147
00:05:52,320 --> 00:05:56,400
There is also PEI dispatcher which is

148
00:05:54,479 --> 00:06:00,000
state machine implemented in in PEI

149
00:05:56,400 --> 00:06:01,520
Foundation which iterates over those

150
00:06:00,000 --> 00:06:03,759
PEI Modules (PEIMs)

151
00:06:01,520 --> 00:06:07,039
and run them and make sure that

152
00:06:03,759 --> 00:06:08,479
those execute in correct order. There

153
00:06:07,039 --> 00:06:12,800
are PEI

154
00:06:08,479 --> 00:06:14,880
services 

155
00:06:12,800 --> 00:06:17,039
which is some set of core

156
00:06:14,880 --> 00:06:20,400
operations used in

157
00:06:17,039 --> 00:06:22,240
a PEIM, so probably some loading of them or

158
00:06:20,400 --> 00:06:24,639


159
00:06:22,240 --> 00:06:24,639
checking

160
00:06:24,720 --> 00:06:30,080
that those execute correctly.

161
00:06:27,199 --> 00:06:31,199
There is also a way to

162
00:06:30,080 --> 00:06:33,199
express

163
00:06:31,199 --> 00:06:34,960
dependency

164
00:06:33,199 --> 00:06:38,960
DEPEX.

165
00:06:34,960 --> 00:06:41,440
So, the each PEI module has set of

166
00:06:38,960 --> 00:06:45,919
GUIDs that must be loaded before

167
00:06:41,440 --> 00:06:47,520
this PEI module can be executed

168
00:06:45,919 --> 00:06:50,960
because it use like previously

169
00:06:47,520 --> 00:06:50,960
established API.

170
00:06:52,400 --> 00:06:58,319
What are the services PEI provide.

171
00:06:56,639 --> 00:07:00,240
So, first of all those are these

172
00:06:58,319 --> 00:07:02,479
framework services which are used by

173
00:07:00,240 --> 00:07:04,240
Foundation, which is loading installing

174
00:07:02,479 --> 00:07:07,360
PPIs

175
00:07:04,240 --> 00:07:10,400
appending information to HOBs or

176
00:07:07,360 --> 00:07:12,960
appending HOBs to the HOBs list

177
00:07:10,400 --> 00:07:13,759
and progressing

178
00:07:12,960 --> 00:07:15,360


179
00:07:13,759 --> 00:07:17,360
through boot process and reporting

180
00:07:15,360 --> 00:07:19,840
that state 

181
00:07:17,360 --> 00:07:23,120
of the progress. There are some ACPI

182
00:07:19,840 --> 00:07:24,880
related services because

183
00:07:23,120 --> 00:07:27,680
we have to detect

184
00:07:24,880 --> 00:07:30,960
from what state we getting back to life

185
00:07:27,680 --> 00:07:34,479
on our platform, is it cold boot, power on,

186
00:07:30,960 --> 00:07:35,520
reboot, just wake up from the sleep and

187
00:07:34,479 --> 00:07:38,240


188
00:07:35,520 --> 00:07:39,759
those ACPI functions help to

189
00:07:38,240 --> 00:07:41,199
detect that.

190
00:07:39,759 --> 00:07:43,599
There are functions related to

191
00:07:41,199 --> 00:07:45,280
triggering resets, there are firmware

192
00:07:43,599 --> 00:07:47,039
volume services of course because we

193
00:07:45,280 --> 00:07:49,199
have to access

194
00:07:47,039 --> 00:07:52,479
next phases and access

195
00:07:49,199 --> 00:07:52,479
storage which is

196
00:07:52,960 --> 00:07:55,840


197
00:07:53,759 --> 00:07:57,919
structured using firmware volumes as

198
00:07:55,840 --> 00:08:00,960
we discussed before and there are some

199
00:07:57,919 --> 00:08:05,840
memory management services.

200
00:08:00,960 --> 00:08:08,240
How the hands-off between PEI to DXE look like?

201
00:08:05,840 --> 00:08:11,039
So, first of all

202
00:08:08,240 --> 00:08:13,120
the finalization of PEI is described in

203
00:08:11,039 --> 00:08:14,720
chapter 9 of platform initialization

204
00:08:13,120 --> 00:08:17,840
specification.

205
00:08:14,720 --> 00:08:19,440
Before DXE started

206
00:08:17,840 --> 00:08:20,720
and there are some crucial things that

207
00:08:19,440 --> 00:08:23,599
have to happen.

208
00:08:20,720 --> 00:08:26,319
First of all we have to find DXE

209
00:08:23,599 --> 00:08:27,599
Foundation code

210
00:08:26,319 --> 00:08:29,599
and then

211
00:08:27,599 --> 00:08:31,759
all the components that needed by DXE

212
00:08:29,599 --> 00:08:33,680
Foundation must be loaded into memory.

213
00:08:31,759 --> 00:08:35,760
Then we have HOBs list which contain

214
00:08:33,680 --> 00:08:37,680
crucial information

215
00:08:35,760 --> 00:08:38,959
required by DXE phase

216
00:08:37,680 --> 00:08:41,519
and

217
00:08:38,959 --> 00:08:44,720
and we have to pass that to DXE

218
00:08:41,519 --> 00:08:47,519
initial program loader (IPL) PPI.

219
00:08:44,720 --> 00:08:49,600
PPI means that this is a

220
00:08:47,519 --> 00:08:50,560
PEIM to PEIM interface so that means

221
00:08:49,600 --> 00:08:52,399
we are

222
00:08:50,560 --> 00:08:55,600
using still some

223
00:08:52,399 --> 00:08:56,399
PEIM functionality, despite the DXE name.

224
00:08:55,600 --> 00:08:58,640


225
00:08:56,399 --> 00:09:02,160
And then this this component finally

226
00:08:58,640 --> 00:09:03,360
starts DXE phase and do the transition

227
00:09:02,160 --> 00:09:04,080
and 

228
00:09:03,360 --> 00:09:07,120


229
00:09:04,080 --> 00:09:09,680
in theory we should never get back.

230
00:09:07,120 --> 00:09:11,120
There are some HOBs required in

231
00:09:09,680 --> 00:09:14,560
specification,

232
00:09:11,120 --> 00:09:16,560
so DXE got some requirements which

233
00:09:14,560 --> 00:09:17,920
expect some information from

234
00:09:16,560 --> 00:09:21,519
previous phase.

235
00:09:17,920 --> 00:09:24,160
First of all is resource descriptor HOB

236
00:09:21,519 --> 00:09:26,959
which inform about established physical

237
00:09:24,160 --> 00:09:28,959
memory. So, we said that PEI is

238
00:09:26,959 --> 00:09:31,760
responsible for memory training memory

239
00:09:28,959 --> 00:09:32,480
initialization and that's one of

240
00:09:31,760 --> 00:09:34,880
the

241
00:09:32,480 --> 00:09:36,800
results of that action.

242
00:09:34,880 --> 00:09:39,200
Then there is

243
00:09:36,800 --> 00:09:41,360
stack location

244
00:09:39,200 --> 00:09:41,360
to

245
00:09:42,320 --> 00:09:45,519
do whatever is needed to either clean

246
00:09:44,240 --> 00:09:48,000
that up,

247
00:09:45,519 --> 00:09:49,120
either reuse some information which are

248
00:09:48,000 --> 00:09:50,880
there.

249
00:09:49,120 --> 00:09:54,000
And of course firmware volumes and

250
00:09:50,880 --> 00:09:56,240
devices related to the storage of

251
00:09:54,000 --> 00:09:58,959
firmware that information is

252
00:09:56,240 --> 00:10:00,000
needed by DXE to continue execution.

253
00:09:58,959 --> 00:10:02,480


254
00:10:00,000 --> 00:10:04,560
There are some platforms specific HOBs

255
00:10:02,480 --> 00:10:05,519
which given vendors

256
00:10:04,560 --> 00:10:07,040
use

257
00:10:05,519 --> 00:10:09,440
but this is not part of the

258
00:10:07,040 --> 00:10:11,360
specification.

259
00:10:09,440 --> 00:10:13,920
How typical

260
00:10:11,360 --> 00:10:15,120
PEI module interface look like?

261
00:10:13,920 --> 00:10:18,720
So, this is

262
00:10:15,120 --> 00:10:22,800
a definition of

263
00:10:18,720 --> 00:10:22,800
PI SMM communication PEIM

264
00:10:22,880 --> 00:10:27,760


265
00:10:24,560 --> 00:10:31,120
PEIM interface

266
00:10:27,760 --> 00:10:34,000
and the and we have the entry point for that

267
00:10:31,120 --> 00:10:35,040
which takes some file handle

268
00:10:34,000 --> 00:10:36,079
and

269
00:10:35,040 --> 00:10:39,440
some

270
00:10:36,079 --> 00:10:41,440
pointer to PEI services.

271
00:10:39,440 --> 00:10:42,800


272
00:10:41,440 --> 00:10:45,279


273
00:10:42,800 --> 00:10:48,160
So, we can see that file handle is meant

274
00:10:45,279 --> 00:10:51,920
only to pass to services

275
00:10:48,160 --> 00:10:51,920
which requires typedef void.




