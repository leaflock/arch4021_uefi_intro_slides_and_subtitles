1
00:00:00,399 --> 00:00:06,160
So, let's start discussing UEFI boot

2
00:00:04,000 --> 00:00:09,440
flow.

3
00:00:06,160 --> 00:00:10,240
You already saw this picture.

4
00:00:09,440 --> 00:00:14,240
We

5
00:00:10,240 --> 00:00:15,440
starting with with the first phase of

6
00:00:14,240 --> 00:00:17,600
UEFI

7
00:00:15,440 --> 00:00:19,920
and PI boot process.

8
00:00:17,600 --> 00:00:22,560
So, first phase defined in platform

9
00:00:19,920 --> 00:00:23,439
initialization specification is

10
00:00:22,560 --> 00:00:25,119
SEC

11
00:00:23,439 --> 00:00:28,160
or security.

12
00:00:25,119 --> 00:00:30,720
So, SEC phase is described in PI

13
00:00:28,160 --> 00:00:31,840
specification chapter 13

14
00:00:30,720 --> 00:00:34,160
and

15
00:00:31,840 --> 00:00:35,760
it is considered root of trust for

16
00:00:34,160 --> 00:00:38,879
UEFI boot flow.

17
00:00:35,760 --> 00:00:41,920
It may authenticate PEI foundation

18
00:00:38,879 --> 00:00:44,719
so in in theory there is no limit

19
00:00:41,920 --> 00:00:47,360
what SEC can authenticate

20
00:00:44,719 --> 00:00:50,480
but in reality it doesn't authenticate

21
00:00:47,360 --> 00:00:51,360
whole PEI phase because PEI phase is

22
00:00:50,480 --> 00:00:54,640


23
00:00:51,360 --> 00:00:57,440
consists of PEI Foundation which is a

24
00:00:54,640 --> 00:01:00,800
component that dispatch various modules

25
00:00:57,440 --> 00:01:02,320
which are inside the PEI phase

26
00:01:00,800 --> 00:01:04,239
and

27
00:01:02,320 --> 00:01:06,799
according to specification

28
00:01:04,239 --> 00:01:09,119
and according to typical implementations.

29
00:01:06,799 --> 00:01:11,360
SEC just authenticates this PI

30
00:01:09,119 --> 00:01:14,240
foundation not the modules which are

31
00:01:11,360 --> 00:01:15,840
executed during the PEI phase.

32
00:01:14,240 --> 00:01:18,080
And because of that various route of

33
00:01:15,840 --> 00:01:21,360
trust technologies like Intel BootGuard

34
00:01:18,080 --> 00:01:24,400
AMD hardware validated boot

35
00:01:21,360 --> 00:01:28,159
just very like when they doing

36
00:01:24,400 --> 00:01:29,600
verification they verify SEC and

37
00:01:28,159 --> 00:01:32,799
PEI phase

38
00:01:29,600 --> 00:01:35,040
including the PEI modules (PEIMs). So, we

39
00:01:32,799 --> 00:01:37,119
already discussed that the SEC

40
00:01:35,040 --> 00:01:40,400
phase and the next phase can be

41
00:01:37,119 --> 00:01:42,720
hardened through some additional mechanism.

42
00:01:40,400 --> 00:01:45,680
SEC starts at reset vector of the

43
00:01:42,720 --> 00:01:48,479
platform which you learned about in

44
00:01:45,680 --> 00:01:51,280
other in previous courses.

45
00:01:48,479 --> 00:01:53,360
So, it includes more most basic

46
00:01:51,280 --> 00:01:56,159
initialization of the processor

47
00:01:53,360 --> 00:01:59,439
and have to perform

48
00:01:56,159 --> 00:02:01,439
switch to protected mode and 

49
00:01:59,439 --> 00:02:02,479
load microcode

50
00:02:01,439 --> 00:02:04,799
and

51
00:02:02,479 --> 00:02:05,600
some sometimes also prepare temporary

52
00:02:04,799 --> 00:02:08,800
RAM,

53
00:02:05,600 --> 00:02:12,560
also called Cache As RAM to be able to

54
00:02:08,800 --> 00:02:12,560
execute more sophisticated code.

55
00:02:12,570 --> 00:02:17,360


56
00:02:13,920 --> 00:02:19,280
It is expected by the spec that

57
00:02:17,360 --> 00:02:20,400
following information will be passed to

58
00:02:19,280 --> 00:02:22,879
the

59
00:02:20,400 --> 00:02:25,120
PEI phase so first of all state of the

60
00:02:22,879 --> 00:02:28,080
platform, second

61
00:02:25,120 --> 00:02:29,200
size of the boot firmware volume,

62
00:02:28,080 --> 00:02:31,280


63
00:02:29,200 --> 00:02:33,200
location and size of the temporary RAM,

64
00:02:31,280 --> 00:02:35,120
location and size of the stack which was

65
00:02:33,200 --> 00:02:38,000
established as a part of

66
00:02:35,120 --> 00:02:38,000
temporary RAM

67
00:02:38,160 --> 00:02:42,480
setup,

68
00:02:39,200 --> 00:02:44,160
and optionally it may pass one or more

69
00:02:42,480 --> 00:02:45,280
hands-off blocks

70
00:02:44,160 --> 00:02:48,840
via

71
00:02:45,280 --> 00:02:50,560
EFI_SEC_HOB_DATA_PPI

72
00:02:48,840 --> 00:02:51,920
structure.

73
00:02:50,560 --> 00:02:54,560


74
00:02:51,920 --> 00:02:57,519
There are also some other HOBs

75
00:02:54,560 --> 00:03:00,000
that can be passed but this 

76
00:02:57,519 --> 00:03:02,480
is optional and also implementation

77
00:03:00,000 --> 00:03:04,959
dependent

78
00:03:02,480 --> 00:03:07,120
what are interesting stuff that

79
00:03:04,959 --> 00:03:08,400
you might learn about SEC

80
00:03:07,120 --> 00:03:12,080
you may find

81
00:03:08,400 --> 00:03:15,360
behind the article where one of 3mdeb

82
00:03:12,080 --> 00:03:16,080
employees trying to enable UEFI boot flow

83
00:03:15,360 --> 00:03:18,879
for

84
00:03:16,080 --> 00:03:20,080
small AllWinner A13

85
00:03:18,879 --> 00:03:23,519
processor,

86
00:03:20,080 --> 00:03:26,799
and it just proves that not all CPUs can

87
00:03:23,519 --> 00:03:28,640
fill requirements of UEFI specification,

88
00:03:26,799 --> 00:03:30,959
which is very interesting because UEFI

89
00:03:28,640 --> 00:03:33,040
specifications should be so flexible and

90
00:03:30,959 --> 00:03:35,519
should give us

91
00:03:33,040 --> 00:03:37,840
ability to enable it on every modern

92
00:03:35,519 --> 00:03:39,599
processor.

93
00:03:37,840 --> 00:03:42,239


94
00:03:39,599 --> 00:03:44,000
So we we may ask ourselves

95
00:03:42,239 --> 00:03:46,640
when there are multiple firmware

96
00:03:44,000 --> 00:03:48,400
volumes what additional code

97
00:03:46,640 --> 00:03:50,720
is required to

98
00:03:48,400 --> 00:03:52,150
start executing

99
00:03:50,720 --> 00:03:53,360
SEC phase.

100
00:03:52,150 --> 00:03:56,000


101
00:03:53,360 --> 00:03:57,840
So, first of all the PI platform

102
00:03:56,000 --> 00:03:59,120
initialization PeiCore foundation

103
00:03:57,840 --> 00:04:00,720
has a mechanism for handling

104
00:03:59,120 --> 00:04:03,840
multiple firmware volumes. And

105
00:04:00,720 --> 00:04:05,920
multiple firmware volumes

106
00:04:03,840 --> 00:04:08,159
is platform

107
00:04:05,920 --> 00:04:09,280
specific and would be described in

108
00:04:08,159 --> 00:04:12,400


109
00:04:09,280 --> 00:04:14,400
special UEFI file called FDF, we saw that

110
00:04:12,400 --> 00:04:16,079
file when we looked at the structure of

111
00:04:14,400 --> 00:04:17,919
the

112
00:04:16,079 --> 00:04:20,000


113
00:04:17,919 --> 00:04:22,639
source tree.

114
00:04:20,000 --> 00:04:25,440
And platform-specific

115
00:04:22,639 --> 00:04:28,560
PEI modules has to declare the location

116
00:04:25,440 --> 00:04:31,440
of the additional firmware volumes

117
00:04:28,560 --> 00:04:36,320
for them to be known to the PEI core.

118
00:04:31,440 --> 00:04:38,960
And once they those

119
00:04:36,320 --> 00:04:41,199
firmware volumes are declared 

120
00:04:38,960 --> 00:04:46,000
to PEI core

121
00:04:41,199 --> 00:04:49,840
and then then PEI core can dispatch

122
00:04:46,000 --> 00:04:50,880
specific PEI modules for them.

123
00:04:49,840 --> 00:04:52,720


124
00:04:50,880 --> 00:04:55,440
And to be honest the only firmware

125
00:04:52,720 --> 00:04:57,440
volume that is known to the PEI core when

126
00:04:55,440 --> 00:05:01,199
it is started

127
00:04:57,440 --> 00:05:02,479
is the boot firmware volume which was

128
00:05:01,199 --> 00:05:07,039
passed

129
00:05:02,479 --> 00:05:07,039
in the information from SEC phase.



