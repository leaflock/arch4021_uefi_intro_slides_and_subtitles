1
00:00:00,480 --> 00:00:05,120
How the UEFI driver model look like? First

2
00:00:03,360 --> 00:00:06,799
of all to understand that we have to

3
00:00:05,120 --> 00:00:09,360
talk about how

4
00:00:06,799 --> 00:00:12,080
UEFI specifications see

5
00:00:09,360 --> 00:00:15,040
modern computing system.

6
00:00:12,080 --> 00:00:17,920
So, UEFI view the system as a set of one

7
00:00:15,040 --> 00:00:18,880
or more processors connected to one or

8
00:00:17,920 --> 00:00:21,600
more

9
00:00:18,880 --> 00:00:22,960
core chipsets, so we can see that we have

10
00:00:21,600 --> 00:00:25,279
some CPU

11
00:00:22,960 --> 00:00:27,119
which is connected to some north bridge

12
00:00:25,279 --> 00:00:29,920
or some chipset.

13
00:00:27,119 --> 00:00:31,439
Then we have a bus, some bus which

14
00:00:29,920 --> 00:00:37,520
connects USB,

15
00:00:31,439 --> 00:00:40,239
and ATA, VGA and behind those

16
00:00:37,520 --> 00:00:41,200
USB controller we have USB bus behind

17
00:00:40,239 --> 00:00:43,680
ATA

18
00:00:41,200 --> 00:00:45,840
we have ATA bus and so on and so on, and

19
00:00:43,680 --> 00:00:47,680
further after that we have

20
00:00:45,840 --> 00:00:50,000
devices.

21
00:00:47,680 --> 00:00:53,520
So, core chipsets are responsible for

22
00:00:50,000 --> 00:00:55,039
producing one or more IO buses so we can

23
00:00:53,520 --> 00:00:56,800
see that

24
00:00:55,039 --> 00:01:00,160
north bridge

25
00:00:56,800 --> 00:01:02,480
in this case produce PCI bus, USB produce

26
00:01:00,160 --> 00:01:04,559
USB bus and so on.

27
00:01:02,480 --> 00:01:09,280
And

28
00:01:04,559 --> 00:01:11,119
the UEFI driver model describes buses

29
00:01:09,280 --> 00:01:14,080
and concentrate mostly on the

30
00:01:11,119 --> 00:01:16,720
communication over some buses, it

31
00:01:14,080 --> 00:01:19,119
does not describe processors or chipsets

32
00:01:16,720 --> 00:01:22,080
because this is more like

33
00:01:19,119 --> 00:01:25,200
a role of the vendor of given

34
00:01:22,080 --> 00:01:27,600
processor or chipset. So, UEFI is

35
00:01:25,200 --> 00:01:30,159
more about what is

36
00:01:27,600 --> 00:01:32,640
communication method for those

37
00:01:30,159 --> 00:01:34,560
components than what

38
00:01:32,640 --> 00:01:36,799
should be inside those components and

39
00:01:34,560 --> 00:01:39,360
how those components work

40
00:01:36,799 --> 00:01:42,320
so in other in other words,

41
00:01:39,360 --> 00:01:44,479
UEFI specification see a modern

42
00:01:42,320 --> 00:01:47,119
computing system as a tree of buses and

43
00:01:44,479 --> 00:01:49,360
devices with core chipsets as a

44
00:01:47,119 --> 00:01:51,520
root of tree. So, core chipset in this

45
00:01:49,360 --> 00:01:53,360
case is in north bridge.

46
00:01:51,520 --> 00:01:55,360
What other thing we can see here on this

47
00:01:53,360 --> 00:01:58,320
diagram is that, the yellow

48
00:01:55,360 --> 00:01:59,119
rectangles are devices so we can see

49
00:01:58,320 --> 00:02:02,159
that

50
00:01:59,119 --> 00:02:04,719
every device controller is connected

51
00:02:02,159 --> 00:02:06,079
through some bus either

52
00:02:04,719 --> 00:02:06,840
directly through

53
00:02:06,079 --> 00:02:08,720
to

54
00:02:06,840 --> 00:02:09,759
northbridge

55
00:02:08,720 --> 00:02:12,160
chipset

56
00:02:09,759 --> 00:02:14,800
or through some other controllers like

57
00:02:12,160 --> 00:02:17,440
USB, ATA or probably many other

58
00:02:14,800 --> 00:02:17,440
controllers.



