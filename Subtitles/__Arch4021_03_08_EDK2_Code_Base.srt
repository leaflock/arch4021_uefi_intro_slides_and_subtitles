1
00:00:00,560 --> 00:00:03,840
So how the

2
00:00:01,839 --> 00:00:06,799
most popular

3
00:00:03,840 --> 00:00:09,280
reference implementation of UEFI

4
00:00:06,799 --> 00:00:12,559
specification look like. So this is

5
00:00:09,280 --> 00:00:14,160
this is a screenshot from the

6
00:00:12,559 --> 00:00:16,960


7
00:00:14,160 --> 00:00:17,680
github. As you can see the

8
00:00:16,960 --> 00:00:19,840


9
00:00:17,680 --> 00:00:22,800
code is quite popular, it got 

10
00:00:19,840 --> 00:00:25,760
almost 3,000 stars like quite a lot of

11
00:00:22,800 --> 00:00:28,480
folks in case of code base it's mostly

12
00:00:25,760 --> 00:00:31,199
ANSI C, it's like

13
00:00:28,480 --> 00:00:33,840
over one and half million lines of code

14
00:00:31,199 --> 00:00:37,120
and there's also some Perl

15
00:00:33,840 --> 00:00:40,239
some Python and some assembler inside.

16
00:00:37,120 --> 00:00:41,680
Those statistics were generated

17
00:00:40,239 --> 00:00:44,480


18
00:00:41,680 --> 00:00:45,600
at the end of 2021. So, when you're

19
00:00:44,480 --> 00:00:46,960
looking this

20
00:00:45,600 --> 00:00:50,559
course

21
00:00:46,960 --> 00:00:50,559
it may look a little bit different.



