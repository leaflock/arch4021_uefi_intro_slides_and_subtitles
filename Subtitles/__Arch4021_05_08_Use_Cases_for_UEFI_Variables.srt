1
00:00:00,000 --> 00:00:04,020
as we already said there are some

2
00:00:01,979 --> 00:00:06,299
existing tools to handle authentication

3
00:00:04,020 --> 00:00:10,139
variables in major operating systems

4
00:00:06,299 --> 00:00:12,360
usually regular users do not touch UEFI

5
00:00:10,139 --> 00:00:14,099
authenticated variables but for firmware

6
00:00:12,360 --> 00:00:16,260
developers and security researchers

7
00:00:14,099 --> 00:00:18,660
authenticated variables are very

8
00:00:16,260 --> 00:00:21,420
important mostly because authenticated

9
00:00:18,660 --> 00:00:24,600
variables provide and help maintain

10
00:00:21,420 --> 00:00:29,220
information about UEFI secure boot status

11
00:00:24,600 --> 00:00:31,859
and keys and signatures default

12
00:00:29,220 --> 00:00:34,739
implementation of UEFI authenticated

13
00:00:31,859 --> 00:00:37,380
variables in EDK2 I use system

14
00:00:34,739 --> 00:00:39,899
management mode as trusted execution

15
00:00:37,380 --> 00:00:43,020
environment which in theory should

16
00:00:39,899 --> 00:00:45,719
provide isolated more secure environment

17
00:00:43,020 --> 00:00:48,300
for updating and creation of the

18
00:00:45,719 --> 00:00:49,640
variables of course this uh this awesome

19
00:00:48,300 --> 00:00:52,379
start

20
00:00:49,640 --> 00:00:55,379
system management mode improves Security

21
00:00:52,379 --> 00:00:58,079
in the past there were some examples of

22
00:00:55,379 --> 00:01:00,059
vulnerabilities vulnerabilities detected

23
00:00:58,079 --> 00:01:02,879
in system management mode what

24
00:01:00,059 --> 00:01:05,640
essentially leads to situation that in

25
00:01:02,879 --> 00:01:08,760
which a default etk2 implementation have

26
00:01:05,640 --> 00:01:11,820
to be adjusted to given design needs

27
00:01:08,760 --> 00:01:14,340
default way that UEFI authenticated

28
00:01:11,820 --> 00:01:16,439
variables are handled is you have to be

29
00:01:14,340 --> 00:01:20,520
supported by by some Hardware features

30
00:01:16,439 --> 00:01:23,460
so if we want to leverage this SMM part

31
00:01:20,520 --> 00:01:26,759
we should have some Hardware features so

32
00:01:23,460 --> 00:01:29,640
for example in Intel chipset there is a

33
00:01:26,759 --> 00:01:33,140
bit that deny Splash access by by

34
00:01:29,640 --> 00:01:37,500
software this bit was described in other

35
00:01:33,140 --> 00:01:40,860
ost2 lecture this is BIOS right protect

36
00:01:37,500 --> 00:01:44,060
protection disable or BIOS right enable

37
00:01:40,860 --> 00:01:48,360
in addition to that there is

38
00:01:44,060 --> 00:01:51,600
eiss-s bit which allows access to SPI

39
00:01:48,360 --> 00:01:55,079
flash only in system management mode so

40
00:01:51,600 --> 00:02:00,000
only with this eiss-s which which is

41
00:01:55,079 --> 00:02:02,720
enabled in SMM SDS the UEFI Authenticator

42
00:02:00,000 --> 00:02:06,119
authenticated variables

43
00:02:02,720 --> 00:02:08,959
implementation in EDK2 can provide

44
00:02:06,119 --> 00:02:11,120
disgust protection

45
00:02:08,959 --> 00:02:14,220
platform

46
00:02:11,120 --> 00:02:18,319
Builders and firmware developers should

47
00:02:14,220 --> 00:02:21,599
should ensure that by any means

48
00:02:18,319 --> 00:02:25,200
that necessary mahines the necessary

49
00:02:21,599 --> 00:02:27,480
Hardware protection mechanisms exist in

50
00:02:25,200 --> 00:02:31,680
the hardware on which we're trying to

51
00:02:27,480 --> 00:02:34,920
store UEFI variables in case of most x86

52
00:02:31,680 --> 00:02:37,500
platforms there is there is SPI

53
00:02:34,920 --> 00:02:40,440
controller which already have required

54
00:02:37,500 --> 00:02:42,540
protection but of course variables can

55
00:02:40,440 --> 00:02:45,000
also use different storage which we

56
00:02:42,540 --> 00:02:46,620
discussed earlier and in that case and

57
00:02:45,000 --> 00:02:48,780
especially in the case when we use

58
00:02:46,620 --> 00:02:50,280
different architecture those Hardware

59
00:02:48,780 --> 00:02:51,840
protection additional Hardware

60
00:02:50,280 --> 00:02:53,879
protection behind it should be checked

61
00:02:51,840 --> 00:02:57,480
authenticated variables provide prevent

62
00:02:53,879 --> 00:03:00,500
only OS level modifications so to

63
00:02:57,480 --> 00:03:03,360
protect protect against physical access

64
00:03:00,500 --> 00:03:06,780
we need to have some additional

65
00:03:03,360 --> 00:03:09,300
mechanism and this we discussed some of

66
00:03:06,780 --> 00:03:12,060
those additional Hardware protection

67
00:03:09,300 --> 00:03:14,640
mechanism at the beginning of of this

68
00:03:12,060 --> 00:03:17,640
section also please note that UEFI

69
00:03:14,640 --> 00:03:21,180
variables do not provide confidentiality

70
00:03:17,640 --> 00:03:24,360
so in regular UEFI authenticated

71
00:03:21,180 --> 00:03:26,940
variables described and implemented

72
00:03:24,360 --> 00:03:29,640
according to fa specification do not

73
00:03:26,940 --> 00:03:31,860
provide confidential ability in general

74
00:03:29,640 --> 00:03:35,159
content of the variable can be read by

75
00:03:31,860 --> 00:03:38,819
anyone but only written by and updated

76
00:03:35,159 --> 00:03:41,420
by the owner of the private key in case

77
00:03:38,819 --> 00:03:43,860
of situation when confidential

78
00:03:41,420 --> 00:03:46,920
confidentiality is important we should

79
00:03:43,860 --> 00:03:49,260
use a user or platform key encrypted

80
00:03:46,920 --> 00:03:51,540
variables those were discussed earlier

81
00:03:49,260 --> 00:03:56,819
in this section and those typically

82
00:03:51,540 --> 00:04:01,280
require some CSME TPM or or some

83
00:03:56,819 --> 00:04:01,280
Hardware security module to be supported

