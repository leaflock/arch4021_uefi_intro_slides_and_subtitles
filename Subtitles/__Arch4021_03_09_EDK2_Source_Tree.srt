1
00:00:00,080 --> 00:00:04,319
Okay let's discuss

2
00:00:01,760 --> 00:00:06,799
EDK2 source tree.

3
00:00:04,319 --> 00:00:07,919
So, on the screen on the left side you

4
00:00:06,799 --> 00:00:10,719
can see

5
00:00:07,919 --> 00:00:11,920
at the main directory of EDK2

6
00:00:10,719 --> 00:00:14,480
sourcetree

7
00:00:11,920 --> 00:00:16,240
and from the top you can

8
00:00:14,480 --> 00:00:18,240
figure out that there are some ARM

9
00:00:16,240 --> 00:00:20,560
packages there are some ARM platform

10
00:00:18,240 --> 00:00:22,640
packages ArmVirt packages

11
00:00:20,560 --> 00:00:24,880
and many others like you can find crypto

12
00:00:22,640 --> 00:00:25,920
packages network packages.

13
00:00:24,880 --> 00:00:27,439


14
00:00:25,920 --> 00:00:29,119
First of all there

15
00:00:27,439 --> 00:00:31,599
are packages and there are also some

16
00:00:29,119 --> 00:00:34,719
other directories like base tools or

17
00:00:31,599 --> 00:00:36,239
conf, which we will discuss in details

18
00:00:34,719 --> 00:00:38,160


19
00:00:36,239 --> 00:00:39,520
in further sections and

20
00:00:38,160 --> 00:00:42,079
subsections.

21
00:00:39,520 --> 00:00:45,200
Typically as you can see the name of

22
00:00:42,079 --> 00:00:49,440
the directory is in my main 

23
00:00:45,200 --> 00:00:50,719
source tree of EDK2 is finished with

24
00:00:49,440 --> 00:00:52,160
pkg

25
00:00:50,719 --> 00:00:55,039
and

26
00:00:52,160 --> 00:00:58,559
what is inside this pkg

27
00:00:55,039 --> 00:01:00,800
UEFI package, we can find based on

28
00:00:58,559 --> 00:01:02,719
example on the right side where we see

29
00:01:00,800 --> 00:01:04,640
UEFI payload package.

30
00:01:02,719 --> 00:01:07,520
And here we got some

31
00:01:04,640 --> 00:01:10,720
modules inside like BlSupportDxe, 

32
00:01:07,520 --> 00:01:14,080
BlSupportSmm and various others. There are

33
00:01:10,720 --> 00:01:16,799
also some include, some libraries

34
00:01:14,080 --> 00:01:21,200
and what is most important are all those

35
00:01:16,799 --> 00:01:22,400
files .dsc, .dec and .fdf

36
00:01:21,200 --> 00:01:26,240
on the bottom.

37
00:01:22,400 --> 00:01:29,360
.dec and .dsc files are needed to

38
00:01:26,240 --> 00:01:31,280
define package those are special files

39
00:01:29,360 --> 00:01:34,640
which got their own

40
00:01:31,280 --> 00:01:35,920
specification as every other file in

41
00:01:34,640 --> 00:01:37,200
in EDK2,

42
00:01:35,920 --> 00:01:39,520
and

43
00:01:37,200 --> 00:01:40,799
those files define what's inside the

44
00:01:39,520 --> 00:01:44,240
package

45
00:01:40,799 --> 00:01:45,360
and we will discuss later

46
00:01:44,240 --> 00:01:47,680
how those

47
00:01:45,360 --> 00:01:51,200
files are created and used.

48
00:01:47,680 --> 00:01:53,600
So, how things look inside the modules

49
00:01:51,200 --> 00:01:55,759
in the EDKII, we have packages,

50
00:01:53,600 --> 00:01:58,719
in the packages we have modules

51
00:01:55,759 --> 00:02:01,600
and on the bottom, we have a list of

52
00:01:58,719 --> 00:02:04,479
SmmAccessDxe module and this module

53
00:02:01,600 --> 00:02:07,520
contains three files. First is a C

54
00:02:04,479 --> 00:02:10,479
source code, second is a header and third

55
00:02:07,520 --> 00:02:10,479
is an .inf file.



