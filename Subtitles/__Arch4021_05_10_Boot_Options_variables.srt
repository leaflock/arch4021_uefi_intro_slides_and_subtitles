1
00:00:00,060 --> 00:00:04,860
let's discuss boot order variables

2
00:00:02,480 --> 00:00:07,919
affili load option data structure

3
00:00:04,860 --> 00:00:10,080
describes format of UEFI boot option

4
00:00:07,919 --> 00:00:12,780
variables as we can see in the top

5
00:00:10,080 --> 00:00:16,920
structure from MDA package UEFI spec

6
00:00:12,780 --> 00:00:20,039
header the structure is is byte packet

7
00:00:16,920 --> 00:00:22,980
buffer of variable land Fields the first

8
00:00:20,039 --> 00:00:26,400
two fields are attributes and five part

9
00:00:22,980 --> 00:00:28,500
list length those are those those have

10
00:00:26,400 --> 00:00:31,859
fixed at length they are declared as a

11
00:00:28,500 --> 00:00:34,680
members of EFI load option structure and

12
00:00:31,859 --> 00:00:37,920
all other fields are variable variable

13
00:00:34,680 --> 00:00:40,379
length fields so let's discuss those

14
00:00:37,920 --> 00:00:43,140
fields for how those are used so first

15
00:00:40,379 --> 00:00:46,200
attributes describe various behavior for

16
00:00:43,140 --> 00:00:48,899
load options for example the attributes

17
00:00:46,200 --> 00:00:51,899
can activate or inactivate given load

18
00:00:48,899 --> 00:00:54,239
option for drivers the attributes may

19
00:00:51,899 --> 00:00:57,660
lead to overriding previously loaded

20
00:00:54,239 --> 00:01:02,160
driver or attributes may also create

21
00:00:57,660 --> 00:01:05,280
groups of load options which can help in

22
00:01:02,160 --> 00:01:07,799
grouping similar options

23
00:01:05,280 --> 00:01:10,140
um one example of that can be load

24
00:01:07,799 --> 00:01:12,420
option which are part of normal boot

25
00:01:10,140 --> 00:01:16,020
process or those which can be chosen

26
00:01:12,420 --> 00:01:18,600
chosen optionally for example by hotkeys

27
00:01:16,020 --> 00:01:21,240
then we have five part list length is

28
00:01:18,600 --> 00:01:23,220
just a length in bytes then we have

29
00:01:21,240 --> 00:01:27,060
description which is human readable

30
00:01:23,220 --> 00:01:29,340
string which is null terminated then we

31
00:01:27,060 --> 00:01:32,100
have file part list this is array of

32
00:01:29,340 --> 00:01:35,820
structures describing location of UEFI

33
00:01:32,100 --> 00:01:38,159
image for a given load option it use fed

34
00:01:35,820 --> 00:01:40,560
device path protocol to describe the

35
00:01:38,159 --> 00:01:43,500
path to the image and finally there is

36
00:01:40,560 --> 00:01:45,540
optional data which is just the binary

37
00:01:43,500 --> 00:01:49,140
data buffer which is passed to the

38
00:01:45,540 --> 00:01:51,659
loaded image if we look at the Fe device

39
00:01:49,140 --> 00:01:54,240
path protocol what we can say about it

40
00:01:51,659 --> 00:01:56,960
is that it can be used on a device

41
00:01:54,240 --> 00:02:00,119
handle to obtain generic path location

42
00:01:56,960 --> 00:02:02,340
those are information which are related

43
00:02:00,119 --> 00:02:05,520
either to physical device or logical

44
00:02:02,340 --> 00:02:08,099
device but the the key point of that

45
00:02:05,520 --> 00:02:12,540
structure is this is help to describe

46
00:02:08,099 --> 00:02:16,620
where physically or logically in in the

47
00:02:12,540 --> 00:02:19,379
system given device is so it contains

48
00:02:16,620 --> 00:02:23,099
three Fields first is type which can be

49
00:02:19,379 --> 00:02:26,940
either Hardware it can be ACPI type of

50
00:02:23,099 --> 00:02:30,480
device path it can be media device part

51
00:02:26,940 --> 00:02:33,599
it can be BIOS boot specification path

52
00:02:30,480 --> 00:02:35,760
or it can also be a special type which

53
00:02:33,599 --> 00:02:38,340
this determine that this is for example

54
00:02:35,760 --> 00:02:41,220
simple end of device path the subtype

55
00:02:38,340 --> 00:02:44,340
highly depends on the type and length

56
00:02:41,220 --> 00:02:47,580
length is self-explanatory and just

57
00:02:44,340 --> 00:02:50,959
contain the length of the of the

58
00:02:47,580 --> 00:02:53,060
structure so let's try to decode the

59
00:02:50,959 --> 00:02:58,440
boot option

60
00:02:53,060 --> 00:03:01,379
used in in our exercises first of all

61
00:02:58,440 --> 00:03:03,900
um let's start with the hex dump of of

62
00:03:01,379 --> 00:03:08,720
the boot option at the beginning we can

63
00:03:03,900 --> 00:03:13,080
see there are UFO variable attributes 7

64
00:03:08,720 --> 00:03:15,420
means four plus two plus one so it means

65
00:03:13,080 --> 00:03:18,060
that the variable is non-volatile

66
00:03:15,420 --> 00:03:19,920
runtime accessible accessible and of

67
00:03:18,060 --> 00:03:22,500
course it have to be a boot time

68
00:03:19,920 --> 00:03:25,739
accessible because runtime requires also

69
00:03:22,500 --> 00:03:28,739
setting up boot time access then we have

70
00:03:25,739 --> 00:03:32,220
load option load option attribute

71
00:03:28,739 --> 00:03:35,280
uh in this case we see it's one and one

72
00:03:32,220 --> 00:03:40,379
means that the option is active then we

73
00:03:35,280 --> 00:03:44,540
have a five part list length and then we

74
00:03:40,379 --> 00:03:48,239
see there is uh there is a utf-16

75
00:03:44,540 --> 00:03:51,200
label for the next two lines which is

76
00:03:48,239 --> 00:03:53,940
null terminated this is our description

77
00:03:51,200 --> 00:03:57,360
which is Humanity table we can read it

78
00:03:53,940 --> 00:04:02,640
on on the right that this is that is EFI

79
00:03:57,360 --> 00:04:06,780
internal shell here then next uh we got

80
00:04:02,640 --> 00:04:07,459
our file path list which contains array

81
00:04:06,780 --> 00:04:11,760
of

82
00:04:07,459 --> 00:04:15,060
EFI device path protocol structures each

83
00:04:11,760 --> 00:04:18,419
EFI device protocol path device path

84
00:04:15,060 --> 00:04:20,639
protocol start with with the type in

85
00:04:18,419 --> 00:04:24,360
this case it is four which is which

86
00:04:20,639 --> 00:04:26,280
means media device path and subtype of

87
00:04:24,360 --> 00:04:27,180
of it is a firmware volume which is

88
00:04:26,280 --> 00:04:31,800
seven

89
00:04:27,180 --> 00:04:35,280
and the length of that is uh hex 14 and

90
00:04:31,800 --> 00:04:39,419
then we got uh the guide of the firmware

91
00:04:35,280 --> 00:04:41,400
volume in this case it is a Dixie Dixie

92
00:04:39,419 --> 00:04:44,699
firmware volume which we already checked

93
00:04:41,400 --> 00:04:48,479
in previous exercises after that

94
00:04:44,699 --> 00:04:51,120
structure we have next EFI device path

95
00:04:48,479 --> 00:04:54,720
protocol structure with type media

96
00:04:51,120 --> 00:04:58,620
device path but now the subtype is six

97
00:04:54,720 --> 00:05:01,560
six means firmware file and of course do

98
00:04:58,620 --> 00:05:06,419
we have our length and for the firmware

99
00:05:01,560 --> 00:05:08,880
file uh subtype we have a good of our

100
00:05:06,419 --> 00:05:12,540
shell application file and finally we

101
00:05:08,880 --> 00:05:17,520
have a special type of EFI device path

102
00:05:12,540 --> 00:05:21,060
protocol which is uh 7f and 7f means

103
00:05:17,520 --> 00:05:23,699
this is end of entire device path

104
00:05:21,060 --> 00:05:27,960
um so it just indicates that there is

105
00:05:23,699 --> 00:05:31,020
there is no further components in our

106
00:05:27,960 --> 00:05:34,259
file path list RI and let's try to

107
00:05:31,020 --> 00:05:36,600
decompose other two UEFI variables which

108
00:05:34,259 --> 00:05:39,780
are very common in every UEFI

109
00:05:36,600 --> 00:05:43,259
implementation so here we see a boot

110
00:05:39,780 --> 00:05:45,180
order variable file which we just hex

111
00:05:43,259 --> 00:05:49,100
dumped and we see the same situation

112
00:05:45,180 --> 00:05:52,620
with variable attribute that it contains

113
00:05:49,100 --> 00:05:55,620
non-volatile runtime and boot time

114
00:05:52,620 --> 00:05:57,600
access that's normal thing for the boot

115
00:05:55,620 --> 00:05:59,460
order because we have we would like to

116
00:05:57,600 --> 00:06:01,740
access that both from the operating

117
00:05:59,460 --> 00:06:05,520
system to change boot order and also

118
00:06:01,740 --> 00:06:07,440
from the boot process BDS level so we

119
00:06:05,520 --> 00:06:11,280
can read the boot order or change the

120
00:06:07,440 --> 00:06:13,860
boot order even from BIOS BIOS menu and

121
00:06:11,280 --> 00:06:16,139
the next components of this variable are

122
00:06:13,860 --> 00:06:19,259
very simple because those are just

123
00:06:16,139 --> 00:06:21,479
numbers there is First Position input

124
00:06:19,259 --> 00:06:23,340
order second position input or their

125
00:06:21,479 --> 00:06:26,580
third position and fourth position of

126
00:06:23,340 --> 00:06:29,400
course if we would change our boot order

127
00:06:26,580 --> 00:06:32,280
we may have situation that on the first

128
00:06:29,400 --> 00:06:35,100
position we have a third option and

129
00:06:32,280 --> 00:06:37,680
that's that's how the boot order works

130
00:06:35,100 --> 00:06:40,800
the boot order describes which of the

131
00:06:37,680 --> 00:06:44,639
boot options they find in previously

132
00:06:40,800 --> 00:06:47,639
described boot variable should be first

133
00:06:44,639 --> 00:06:51,240
second or third or fourth and of course

134
00:06:47,639 --> 00:06:54,660
would order is consumed by boot manager

135
00:06:51,240 --> 00:06:58,259
during the boot process there is other

136
00:06:54,660 --> 00:07:01,979
interesting variable called timeout this

137
00:06:58,259 --> 00:07:06,720
variable just tells us how long system

138
00:07:01,979 --> 00:07:08,699
waits for entering the boot money and in

139
00:07:06,720 --> 00:07:11,880
this case there is no way there is just

140
00:07:08,699 --> 00:07:15,060
immediately execution of the boot

141
00:07:11,880 --> 00:07:17,460
process because the value of of this

142
00:07:15,060 --> 00:07:20,220
timeout is zero as you can see both

143
00:07:17,460 --> 00:07:21,139
values are regular UEFI variables those

144
00:07:20,220 --> 00:07:23,280
are no

145
00:07:21,139 --> 00:07:26,099
authenticated variables so those are

146
00:07:23,280 --> 00:07:29,840
unprotected and the most most operating

147
00:07:26,099 --> 00:07:33,740
system provide ready to use tools to

148
00:07:29,840 --> 00:07:33,740
manipulate those variables

