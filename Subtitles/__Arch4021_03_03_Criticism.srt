1
00:00:00,080 --> 00:00:04,880
UEFI also

2
00:00:01,599 --> 00:00:07,680
gets quite a lot of criticism,

3
00:00:04,880 --> 00:00:11,840
so one of the 

4
00:00:07,680 --> 00:00:15,040
main point of criticizing 

5
00:00:11,840 --> 00:00:16,800
UEFI specification was its complexity

6
00:00:15,040 --> 00:00:19,680
but

7
00:00:16,800 --> 00:00:22,160
who did this criticism like most of

8
00:00:19,680 --> 00:00:24,960
the digital rights and privacy

9
00:00:22,160 --> 00:00:27,359
activists like Ron Minnich, coreboot

10
00:00:24,960 --> 00:00:30,080
creator like LinuxBIOS creator and

11
00:00:27,359 --> 00:00:33,040
which this project evolved into coreboot

12
00:00:30,080 --> 00:00:35,280
later. Matthew Garrett who at

13
00:00:33,040 --> 00:00:37,760
the beginning was

14
00:00:35,280 --> 00:00:40,800
very concerned about secure boot and

15
00:00:37,760 --> 00:00:42,559
did quite a lot of work around secure

16
00:00:40,800 --> 00:00:46,559
boot and making

17
00:00:42,559 --> 00:00:47,440
it's available for Linux environment.

18
00:00:46,559 --> 00:00:48,320


19
00:00:47,440 --> 00:00:51,039
So,

20
00:00:48,320 --> 00:00:52,719
Ron typically

21
00:00:51,039 --> 00:00:54,719
argued that

22
00:00:52,719 --> 00:00:56,640
UEFI does not follow

23
00:00:54,719 --> 00:00:57,680
and does not solve the problem of

24
00:00:56,640 --> 00:00:59,199
drivers

25
00:00:57,680 --> 00:01:01,280
redundancy.

26
00:00:59,199 --> 00:01:04,400
So that means 

27
00:01:01,280 --> 00:01:06,640
UEFI breaks the do not repeat yourself rule

28
00:01:04,400 --> 00:01:07,760
which is well known in open source

29
00:01:06,640 --> 00:01:09,600
community.

30
00:01:07,760 --> 00:01:12,479
Because of

31
00:01:09,600 --> 00:01:14,000
UEFI drivers and whole big ecosystem

32
00:01:12,479 --> 00:01:17,200
that have to be implemented and

33
00:01:14,000 --> 00:01:19,200
maintained over time, it means that

34
00:01:17,200 --> 00:01:21,119
operating system also needs the write

35
00:01:19,200 --> 00:01:22,240
drivers because those are

36
00:01:21,119 --> 00:01:24,240
two different

37
00:01:22,240 --> 00:01:26,960
programming environments,

38
00:01:24,240 --> 00:01:29,200
And in that we have to overall

39
00:01:26,960 --> 00:01:32,079
maintain multiple drivers for

40
00:01:29,200 --> 00:01:33,759
single device for one hardware and that

41
00:01:32,079 --> 00:01:36,560
is very bad for

42
00:01:33,759 --> 00:01:40,159
efficiency of the

43
00:01:36,560 --> 00:01:43,040
efficient use of programming

44
00:01:40,159 --> 00:01:44,880
resources that we have.

45
00:01:43,040 --> 00:01:47,439
The other

46
00:01:44,880 --> 00:01:50,000
criticism was that UEFI

47
00:01:47,439 --> 00:01:52,720
secure boot effectively prevents platform

48
00:01:50,000 --> 00:01:55,840
reownership and

49
00:01:52,720 --> 00:01:58,320
promote the vendor lock-in. So yes that

50
00:01:55,840 --> 00:02:02,240
the UEFI forum design secure boot mechanism

51
00:01:58,320 --> 00:02:03,840
is to favor more OEM than end user and

52
00:02:02,240 --> 00:02:06,479
because of that

53
00:02:03,840 --> 00:02:09,280
the scheme

54
00:02:06,479 --> 00:02:12,400
should help OEMs providing security

55
00:02:09,280 --> 00:02:15,920
features for end users but does not

56
00:02:12,400 --> 00:02:18,480
always work as expected and

57
00:02:15,920 --> 00:02:19,680
because of that it is very hard to

58
00:02:18,480 --> 00:02:22,640
re-own

59
00:02:19,680 --> 00:02:25,120
correctly provision a

60
00:02:22,640 --> 00:02:26,879
hardware with secure boot enabled and

61
00:02:25,120 --> 00:02:30,160
and various other

62
00:02:26,879 --> 00:02:35,360
root of trust technologies there.

63
00:02:30,160 --> 00:02:37,519
And the scheme of certificates and

64
00:02:35,360 --> 00:02:41,200
and verification process for

65
00:02:37,519 --> 00:02:44,400
UEFI secure boot caused some serious

66
00:02:41,200 --> 00:02:47,040
vulnerabilities. One of the biggest

67
00:02:44,400 --> 00:02:50,000
and recent one was BootHole which was

68
00:02:47,040 --> 00:02:51,120
critical vulnerability in GRUB2 and it

69
00:02:50,000 --> 00:02:54,239
was

70
00:02:51,120 --> 00:02:56,959
it was specifically related to

71
00:02:54,239 --> 00:02:59,120
certificates are revoked how

72
00:02:56,959 --> 00:03:01,120
ecosystem of

73
00:02:59,120 --> 00:03:03,280
certificates for secure boot is

74
00:03:01,120 --> 00:03:04,400
maintained.

75
00:03:03,280 --> 00:03:06,560
There are multiple

76
00:03:04,400 --> 00:03:10,239
vendor-specific implementations based on

77
00:03:06,560 --> 00:03:12,400
common reference code which is EDK2

78
00:03:10,239 --> 00:03:13,920
this means that

79
00:03:12,400 --> 00:03:16,879
often the

80
00:03:13,920 --> 00:03:19,920
mistakes and the vulnerabilities [patches] found

81
00:03:16,879 --> 00:03:22,720
in various places are not propagated

82
00:03:19,920 --> 00:03:25,680
across ecosystem, they are not backported

83
00:03:22,720 --> 00:03:27,599
because there is no well known well

84
00:03:25,680 --> 00:03:28,799
supported mechanism to backport things

85
00:03:27,599 --> 00:03:30,959
and also

86
00:03:28,799 --> 00:03:32,879
many vendors consider their code

87
00:03:30,959 --> 00:03:35,440
proprietary and they don't want to open

88
00:03:32,879 --> 00:03:38,080
source that, so there is no need for them

89
00:03:35,440 --> 00:03:39,760
to backport that to the open source

90
00:03:38,080 --> 00:03:41,680
common code.

91
00:03:39,760 --> 00:03:44,080
On the other side when common code moves

92
00:03:41,680 --> 00:03:47,360
forward it is not always

93
00:03:44,080 --> 00:03:49,519
updated inside the closed source

94
00:03:47,360 --> 00:03:51,840
implementations.

95
00:03:49,519 --> 00:03:54,799
But main goal of UEFI

96
00:03:51,840 --> 00:03:57,439
is preserved, so long lasting firmware

97
00:03:54,799 --> 00:03:59,439
supply chain works the same as it was

98
00:03:57,439 --> 00:04:02,000
during the BIOS time.

99
00:03:59,439 --> 00:04:04,560
It is not transparent and effectively

100
00:04:02,000 --> 00:04:07,040
prevent building trustworthy relation

101
00:04:04,560 --> 00:04:09,680
between end user and the vendor

102
00:04:07,040 --> 00:04:13,120
because the the goal is to build correct

103
00:04:09,680 --> 00:04:15,040
relation between silicon vendor and

104
00:04:13,120 --> 00:04:16,239
and OEM.

105
00:04:15,040 --> 00:04:18,320
So,

106
00:04:16,239 --> 00:04:21,680
other point is that

107
00:04:18,320 --> 00:04:24,479
UEFI specification claim to be minimal

108
00:04:21,680 --> 00:04:25,840
set of flexible interfaces but it's ever

109
00:04:24,479 --> 00:04:28,080
growing,

110
00:04:25,840 --> 00:04:30,560
we just see a growing number of

111
00:04:28,080 --> 00:04:33,840
interfaces defined in the spec

112
00:04:30,560 --> 00:04:36,560
and major OEMs

113
00:04:33,840 --> 00:04:37,919
adds their own protocol and it's

114
00:04:36,560 --> 00:04:39,600
also

115
00:04:37,919 --> 00:04:42,479
all the time growing.

116
00:04:39,600 --> 00:04:44,960
While we agree, 

117
00:04:42,479 --> 00:04:47,840
it can be a good goal to use minimal

118
00:04:44,960 --> 00:04:49,360
interfaces the reality shows something

119
00:04:47,840 --> 00:04:51,759
completely different.

120
00:04:49,360 --> 00:04:54,400
UEFI firmware providers

121
00:04:51,759 --> 00:04:56,720
tend to blow [up] the images with many

122
00:04:54,400 --> 00:04:58,160
interfaces which result in copies of the

123
00:04:56,720 --> 00:05:00,479
interface

124
00:04:58,160 --> 00:05:04,479
either inside SMM

125
00:05:00,479 --> 00:05:06,560
either in other places. And this is

126
00:05:04,479 --> 00:05:10,240
just

127
00:05:06,560 --> 00:05:12,320
bloating whole firmware environment.

128
00:05:10,240 --> 00:05:15,520
If we consider only the top level

129
00:05:12,320 --> 00:05:18,560
interfaces defined by UEFI spec

130
00:05:15,520 --> 00:05:19,360
and would not care about next level

131
00:05:18,560 --> 00:05:21,039


132
00:05:19,360 --> 00:05:23,199
calls

133
00:05:21,039 --> 00:05:25,280
to other interfaces,

134
00:05:23,199 --> 00:05:28,400
we can then say maybe those are a

135
00:05:25,280 --> 00:05:30,400
minimal because like those higher layer

136
00:05:28,400 --> 00:05:32,160
calls,

137
00:05:30,400 --> 00:05:35,039
there are just bunch of them.

138
00:05:32,160 --> 00:05:36,720
But, behind the scene there is much

139
00:05:35,039 --> 00:05:38,880
more

140
00:05:36,720 --> 00:05:41,440
and support for wide range of underlying

141
00:05:38,880 --> 00:05:42,400
hardware to often justify adding UEFI

142
00:05:41,440 --> 00:05:44,960
modules

143
00:05:42,400 --> 00:05:47,199
that have no right to be supported on

144
00:05:44,960 --> 00:05:50,240
some on some platforms. So, for example

145
00:05:47,199 --> 00:05:52,639
we're getting our

146
00:05:50,240 --> 00:05:54,880
BIOS image with multiple components

147
00:05:52,639 --> 00:05:56,880
which are completely not needed for our

148
00:05:54,880 --> 00:05:59,199
platform because for example some

149
00:05:56,880 --> 00:06:02,000
hardware features are not supported but

150
00:05:59,199 --> 00:06:04,400
we're still getting that binary inside

151
00:06:02,000 --> 00:06:08,240
and this just increase our

152
00:06:04,400 --> 00:06:10,639
potential trusted computing base.

153
00:06:08,240 --> 00:06:13,919
So, probably the biggest issue of UEFI is

154
00:06:10,639 --> 00:06:16,319
its complexity in long run 

155
00:06:13,919 --> 00:06:18,000
UEFI implementations are are quite hard

156
00:06:16,319 --> 00:06:20,479
to maintain

157
00:06:18,000 --> 00:06:22,479
because of the complex supply chain

158
00:06:20,479 --> 00:06:25,280
and of course

159
00:06:22,479 --> 00:06:26,080
complexity implies

160
00:06:25,280 --> 00:06:28,720


161
00:06:26,080 --> 00:06:30,000
problems in area of performance and

162
00:06:28,720 --> 00:06:31,840
security.

163
00:06:30,000 --> 00:06:35,520
So, there are multiple opinions from

164
00:06:31,840 --> 00:06:37,600
various people like even Linus Torvalds

165
00:06:35,520 --> 00:06:40,400
said that

166
00:06:37,600 --> 00:06:42,560
UEFI is worse than BIOS in every

167
00:06:40,400 --> 00:06:43,840
aspect.

168
00:06:42,560 --> 00:06:45,759
But

169
00:06:43,840 --> 00:06:47,199
on the other side,

170
00:06:45,759 --> 00:06:48,479
we finally have some standard

171
00:06:47,199 --> 00:06:51,280
specification

172
00:06:48,479 --> 00:06:54,080
to which we can

173
00:06:51,280 --> 00:06:54,080
comply.



