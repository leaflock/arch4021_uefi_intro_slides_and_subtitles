1
00:00:00,060 --> 00:00:06,839
let's talk a little bit about data flow

2
00:00:03,780 --> 00:00:09,000
one using EFI variable authentication to

3
00:00:06,839 --> 00:00:11,760
descriptor so diagram on this slide

4
00:00:09,000 --> 00:00:15,080
present complexity of UEFI authenticated

5
00:00:11,760 --> 00:00:18,960
variables which use authentication to

6
00:00:15,080 --> 00:00:22,800
descriptor it shows just set variable

7
00:00:18,960 --> 00:00:26,100
color site how to prepare everything how

8
00:00:22,800 --> 00:00:29,400
to prepare all components that go as a

9
00:00:26,100 --> 00:00:31,500
parameters to set variable function so

10
00:00:29,400 --> 00:00:33,239
in this case the color is responsible

11
00:00:31,500 --> 00:00:36,320
for Gathering all required information

12
00:00:33,239 --> 00:00:41,300
like hashes signatures

13
00:00:36,320 --> 00:00:44,760
constructing pkcs7 and structure and

14
00:00:41,300 --> 00:00:47,760
concatenating it with the data that is

15
00:00:44,760 --> 00:00:50,820
used to update the value of variable or

16
00:00:47,760 --> 00:00:54,660
to set to to provide initial value of

17
00:00:50,820 --> 00:00:57,420
the variable and at final step whole

18
00:00:54,660 --> 00:00:59,000
whole data whole concatenated data is

19
00:00:57,420 --> 00:01:02,820
passed to the function

20
00:00:59,000 --> 00:01:04,500
uh so we have to go through hold this

21
00:01:02,820 --> 00:01:06,360
process every time we want to create

22
00:01:04,500 --> 00:01:08,520
authenticated variable or update

23
00:01:06,360 --> 00:01:11,159
authenticated variables so let's let's

24
00:01:08,520 --> 00:01:14,040
try to do that uh right now so first

25
00:01:11,159 --> 00:01:17,100
thing is on the top is the creation of

26
00:01:14,040 --> 00:01:20,520
the EFI variable authentication to

27
00:01:17,100 --> 00:01:24,740
descriptor then we hashing a variable

28
00:01:20,520 --> 00:01:28,320
name vendor GUID attributes timestamp

29
00:01:24,740 --> 00:01:30,720
and new content of our variable we

30
00:01:28,320 --> 00:01:34,140
signed the digest that we that we get

31
00:01:30,720 --> 00:01:36,900
and then we construct a their encoded

32
00:01:34,140 --> 00:01:40,920
Signet data structure which have to be

33
00:01:36,900 --> 00:01:44,060
compliant with pkcs7 version 1.5 and

34
00:01:40,920 --> 00:01:47,820
then we fill the EFI variable

35
00:01:44,060 --> 00:01:51,299
authentication to descriptor the out

36
00:01:47,820 --> 00:01:54,360
info uh part the third data with this

37
00:01:51,299 --> 00:01:57,360
signed data that we that we received

38
00:01:54,360 --> 00:02:00,180
here in that encoded format then we have

39
00:01:57,360 --> 00:02:03,420
to concatenate the serialized

40
00:02:00,180 --> 00:02:07,459
authentication to the scriptor with the

41
00:02:03,420 --> 00:02:10,580
new variable content and then we can

42
00:02:07,459 --> 00:02:14,239
provide parameters to set variable

43
00:02:10,580 --> 00:02:18,000
variable name vendor code attributes

44
00:02:14,239 --> 00:02:21,000
data size and then data as our

45
00:02:18,000 --> 00:02:22,920
concatenated serialized content luckily

46
00:02:21,000 --> 00:02:25,080
there are there are some tools to

47
00:02:22,920 --> 00:02:27,620
automate that process for example in

48
00:02:25,080 --> 00:02:30,060
Linux we have SB sign tools

49
00:02:27,620 --> 00:02:33,200
unfortunately those tools are not always

50
00:02:30,060 --> 00:02:35,400
well maintained what may indicate that

51
00:02:33,200 --> 00:02:37,980
authenticated variables are not used

52
00:02:35,400 --> 00:02:40,260
very often what may mean some

53
00:02:37,980 --> 00:02:42,420
opportunities for security researchers

54
00:02:40,260 --> 00:02:44,640
but also this means that the

55
00:02:42,420 --> 00:02:47,519
authenticated variables are undervalued

56
00:02:44,640 --> 00:02:50,160
and maybe we should educate Community to

57
00:02:47,519 --> 00:02:53,280
to use that so if you think this this

58
00:02:50,160 --> 00:02:56,519
flow seem to be complex please wait for

59
00:02:53,280 --> 00:02:59,400
authentication 3 descriptor flow it is

60
00:02:56,519 --> 00:03:00,780
even more complex we will try to play a

61
00:02:59,400 --> 00:03:04,940
little bit with authentication to

62
00:03:00,780 --> 00:03:08,239
descriptor in some practical exercises

63
00:03:04,940 --> 00:03:11,640
unfortunately because of lack of usage

64
00:03:08,239 --> 00:03:16,019
for authentication 3 we will we'll just

65
00:03:11,640 --> 00:03:21,200
omit that so wait for practice exercises

66
00:03:16,019 --> 00:03:21,200
to use authentication to a descriptor

