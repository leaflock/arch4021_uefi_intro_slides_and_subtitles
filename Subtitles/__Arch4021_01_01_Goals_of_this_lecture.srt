1
00:00:00,539 --> 00:00:06,720
hello and welcome into Arc 4021

2
00:00:04,140 --> 00:00:08,639
introductory UEFI my name is Scott cruel

3
00:00:06,720 --> 00:00:11,160
and I will be a trainer during this

4
00:00:08,639 --> 00:00:12,960
class what are the goals of this class

5
00:00:11,160 --> 00:00:15,120
first of all we will learn about

6
00:00:12,960 --> 00:00:17,880
firmware fundamentals what is firmware

7
00:00:15,120 --> 00:00:20,400
what is its purpose uh why we need it

8
00:00:17,880 --> 00:00:22,580
where is it stored and how it relates

9
00:00:20,400 --> 00:00:25,140
to term BIOS

10
00:00:22,580 --> 00:00:27,420
then we will talk a little bit about the

11
00:00:25,140 --> 00:00:31,260
history of BIOS and how it evolved into

12
00:00:27,420 --> 00:00:34,380
into UEFI also we will discuss little bit

13
00:00:31,260 --> 00:00:38,100
about jargon about the terms BIOS versus

14
00:00:34,380 --> 00:00:40,620
UEFI BIOS various versus UEFI who use it

15
00:00:38,100 --> 00:00:42,780
and why and why sometimes it may be

16
00:00:40,620 --> 00:00:47,219
controversial then we will talk a little

17
00:00:42,780 --> 00:00:49,140
bit about UEFI Basics starting with goals

18
00:00:47,219 --> 00:00:50,719
of the UEFI specification and Pi

19
00:00:49,140 --> 00:00:53,700
specification

20
00:00:50,719 --> 00:00:56,760
essential components described in both

21
00:00:53,700 --> 00:00:59,039
of those specifications then we will

22
00:00:56,760 --> 00:01:03,120
talk about history how the specification

23
00:00:59,039 --> 00:01:06,000
evolved and um and we will close

24
00:01:03,120 --> 00:01:08,400
discussion about Basics with talking

25
00:01:06,000 --> 00:01:10,860
about implementations especially

26
00:01:08,400 --> 00:01:14,040
reference implementation called EDK 2

27
00:01:10,860 --> 00:01:15,960
and criticism what are the concerns of

28
00:01:14,040 --> 00:01:17,040
people that criticize your UEFI

29
00:01:15,960 --> 00:01:19,680
specification

30
00:01:17,040 --> 00:01:22,140
then we will switch into UEFI boot flow

31
00:01:19,680 --> 00:01:25,200
where we will describe various booting

32
00:01:22,140 --> 00:01:27,240
stages of modern Computing system uh

33
00:01:25,200 --> 00:01:29,100
especially those described in UEFI

34
00:01:27,240 --> 00:01:33,240
specification and Pi specification

35
00:01:29,100 --> 00:01:36,780
called SEC, PEI, DXE and BDS

36
00:01:33,240 --> 00:01:39,659
in next section we will talk about UEFI

37
00:01:36,780 --> 00:01:41,939
variables which are critical component

38
00:01:39,659 --> 00:01:45,840
to understand more complex topics like

39
00:01:41,939 --> 00:01:47,400
UEFI secure boot so in UEFI variables

40
00:01:45,840 --> 00:01:50,100
section we will learn about the

41
00:01:47,400 --> 00:01:52,200
attribute of, attributes of UEFI variables

42
00:01:50,100 --> 00:01:53,939
at what kind of capabilities they

43
00:01:52,200 --> 00:01:56,159
provide and we will talk about

44
00:01:53,939 --> 00:01:59,340
authenticated variables which are very

45
00:01:56,159 --> 00:02:03,560
important security concept used in in

46
00:01:59,340 --> 00:02:07,220
UEFI and we will close with

47
00:02:03,560 --> 00:02:10,619
analyzing how to access UEFI variables in

48
00:02:07,220 --> 00:02:14,280
Linux environment from the Practical

49
00:02:10,619 --> 00:02:16,560
point of view we will build debug and

50
00:02:14,280 --> 00:02:19,440
look into structures of

51
00:02:16,560 --> 00:02:22,440
um of EDK2 reference implementation of

52
00:02:19,440 --> 00:02:26,459
UEFI we will analyze bootlog to

53
00:02:22,440 --> 00:02:28,680
understand uh boot phases of UEFI we will

54
00:02:26,459 --> 00:02:32,340
look into UEFI shell and commands use it

55
00:02:28,680 --> 00:02:35,040
in UEFI Shell we will look into self UEFI

56
00:02:32,340 --> 00:02:38,400
self certification test so to understand

57
00:02:35,040 --> 00:02:40,980
how UEFI is tested and we will end up

58
00:02:38,400 --> 00:02:45,480
with UEFI variables trying to create

59
00:02:40,980 --> 00:02:48,239
modify various variables and see how

60
00:02:45,480 --> 00:02:50,840
this works finally you may ask yourself

61
00:02:48,239 --> 00:02:52,980
why should I take this class

62
00:02:50,840 --> 00:02:54,660
first of all it depends on your

63
00:02:52,980 --> 00:02:56,160
situation but if you are a firmware

64
00:02:54,660 --> 00:02:59,280
developer maybe you are at the beginning

65
00:02:56,160 --> 00:03:02,000
of your career part career path so it is

66
00:02:59,280 --> 00:03:05,780
very good start to understand

67
00:03:02,000 --> 00:03:09,420
top top most most widely adopted

68
00:03:05,780 --> 00:03:11,159
firmware specification which is which is

69
00:03:09,420 --> 00:03:14,040
one which we will discuss in this course

70
00:03:11,159 --> 00:03:16,800
so this is very good first first step to

71
00:03:14,040 --> 00:03:17,700
understand to start your career maybe

72
00:03:16,800 --> 00:03:20,519
you

73
00:03:17,700 --> 00:03:22,860
you are not familiar with UEFI but you

74
00:03:20,519 --> 00:03:25,260
are a female developer so you can extend

75
00:03:22,860 --> 00:03:27,120
your knowledge through uh through this

76
00:03:25,260 --> 00:03:29,580
course you may be also security

77
00:03:27,120 --> 00:03:32,760
researchers looking for understanding of

78
00:03:29,580 --> 00:03:36,000
firmware to do your job better to find

79
00:03:32,760 --> 00:03:40,080
to to find vulnerabilities to protect

80
00:03:36,000 --> 00:03:42,659
firmware uh that that you have design

81
00:03:40,080 --> 00:03:45,599
um maybe you are also you are just

82
00:03:42,659 --> 00:03:47,519
curious you just want to understand how

83
00:03:45,599 --> 00:03:49,620
things work and you understand the

84
00:03:47,519 --> 00:03:53,760
nitty-gritty uh

85
00:03:49,620 --> 00:03:57,299
Concepts behind booting process this is

86
00:03:53,760 --> 00:03:59,060
also of course which will explain those

87
00:03:57,299 --> 00:04:02,780
those Concepts

88
00:03:59,060 --> 00:04:05,780
this course was also also designed to

89
00:04:02,780 --> 00:04:08,340
prepare you for more

90
00:04:05,780 --> 00:04:11,459
advanced topics as I said you'll find

91
00:04:08,340 --> 00:04:14,700
secure boot but also Deep dive into UEFI

92
00:04:11,459 --> 00:04:17,400
internals uh also Hardware Hands-On of

93
00:04:14,700 --> 00:04:21,000
of UEFI with the goal of creating your

94
00:04:17,400 --> 00:04:23,180
own UEFI or bringing bringing firmware to

95
00:04:21,000 --> 00:04:25,740
completely new hardware

96
00:04:23,180 --> 00:04:29,400
and last thing I would like to mention

97
00:04:25,740 --> 00:04:31,740
is uh that UEFI is not the only way of

98
00:04:29,400 --> 00:04:35,520
booting a Computing platform not the

99
00:04:31,740 --> 00:04:37,199
only framework which can help us do that

100
00:04:35,520 --> 00:04:39,180
there is also core boot which is

101
00:04:37,199 --> 00:04:42,180
firmware framework also called open

102
00:04:39,180 --> 00:04:45,120
source BIOS and there is a lecture

103
00:04:42,180 --> 00:04:47,699
dedicated to that collect Arc for 4031

104
00:04:45,120 --> 00:04:51,120
knowing multiple implementations and

105
00:04:47,699 --> 00:04:53,160
multiple approach approaches to to the

106
00:04:51,120 --> 00:04:55,440
boot process

107
00:04:53,160 --> 00:04:58,080
definitely will help you build more

108
00:04:55,440 --> 00:05:00,479
reliable and secure firmware

109
00:04:58,080 --> 00:05:03,000
as a last word I would like to wish you

110
00:05:00,479 --> 00:05:05,900
good luck with the course and hope hope

111
00:05:03,000 --> 00:05:05,900
you will enjoy it

