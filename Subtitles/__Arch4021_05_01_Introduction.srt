1
00:00:00,599 --> 00:00:07,020
welcome to UEFI variables in this section

2
00:00:03,959 --> 00:00:10,320
we will learn about key components used

3
00:00:07,020 --> 00:00:14,219
to keep user configuration data in UEFI

4
00:00:10,320 --> 00:00:18,060
BIOS filmon so UEFI variables can control

5
00:00:14,219 --> 00:00:20,760
firmware Behavior but on other side have

6
00:00:18,060 --> 00:00:22,740
to be updateable and adjustable by end

7
00:00:20,760 --> 00:00:25,380
user because of those different

8
00:00:22,740 --> 00:00:28,619
properties protection of UEFI variables

9
00:00:25,380 --> 00:00:30,599
is different than protection of UEFI BIOS

10
00:00:28,619 --> 00:00:32,660
code and we have to take that into

11
00:00:30,599 --> 00:00:35,820
consideration When

12
00:00:32,660 --> 00:00:38,880
developing a firmware when the designing

13
00:00:35,820 --> 00:00:40,860
platforms so we know that UEFI variables

14
00:00:38,880 --> 00:00:43,800
are used to store UEFI firmware

15
00:00:40,860 --> 00:00:45,660
configuration data but what are the

16
00:00:43,800 --> 00:00:48,120
examples of of such data such

17
00:00:45,660 --> 00:00:51,660
configuration data so one good example

18
00:00:48,120 --> 00:00:53,820
is boot order so boot order we typically

19
00:00:51,660 --> 00:00:56,699
experience every time we put our

20
00:00:53,820 --> 00:00:59,039
computer and it choose the boot media

21
00:00:56,699 --> 00:01:02,340
either it starts from Network either it

22
00:00:59,039 --> 00:01:05,760
starts from USB or it starts from some

23
00:01:02,340 --> 00:01:09,240
some disk and other examples are Keys

24
00:01:05,760 --> 00:01:11,400
used for UEFI secure Boot provisioning

25
00:01:09,240 --> 00:01:13,560
and configuration like platform key or

26
00:01:11,400 --> 00:01:18,060
key exchange key and we will learn about

27
00:01:13,560 --> 00:01:21,119
those in other ost2 lectures variables

28
00:01:18,060 --> 00:01:23,520
are defined as a key value pairs the key

29
00:01:21,119 --> 00:01:25,680
consists of identifying information and

30
00:01:23,520 --> 00:01:28,680
attributes so inferring information can

31
00:01:25,680 --> 00:01:31,259
be good or some name and attributes are

32
00:01:28,680 --> 00:01:34,259
will be discussed later those are the

33
00:01:31,259 --> 00:01:36,720
crucial part of how UEFI variables are

34
00:01:34,259 --> 00:01:38,880
treated by the UEFI implementation and

35
00:01:36,720 --> 00:01:40,860
the second part is the value and value

36
00:01:38,880 --> 00:01:43,560
is typically some arbitrary data

37
00:01:40,860 --> 00:01:45,720
typically in binary format UEFI

38
00:01:43,560 --> 00:01:48,960
specification does not cover variable

39
00:01:45,720 --> 00:01:51,479
storage media requirements and and

40
00:01:48,960 --> 00:01:54,299
security related to that media so it is

41
00:01:51,479 --> 00:01:57,240
up to the platform designer to provide

42
00:01:54,299 --> 00:02:00,780
some mahoneys to protect media and to

43
00:01:57,240 --> 00:02:02,939
make it available in in a way that that

44
00:02:00,780 --> 00:02:06,299
UEFI BIOS can use it

45
00:02:02,939 --> 00:02:09,119
so but what kind of capabilities this

46
00:02:06,299 --> 00:02:11,039
storage have to have so most variables

47
00:02:09,119 --> 00:02:14,340
will have to be persistent across the

48
00:02:11,039 --> 00:02:16,800
reboots and so so but of course we can

49
00:02:14,340 --> 00:02:19,200
imagine variables which are not

50
00:02:16,800 --> 00:02:21,660
persistent which which are just passed

51
00:02:19,200 --> 00:02:23,099
from firmware created in firmware

52
00:02:21,660 --> 00:02:25,560
password from firmware to operating

53
00:02:23,099 --> 00:02:29,520
system but most of the variables will

54
00:02:25,560 --> 00:02:31,319
require persistence so system also have

55
00:02:29,520 --> 00:02:34,020
to be designed in a way that variables

56
00:02:31,319 --> 00:02:36,500
are available each time the system boot

57
00:02:34,020 --> 00:02:40,620
so using removable storage should be

58
00:02:36,500 --> 00:02:44,340
avoided and of course because the

59
00:02:40,620 --> 00:02:47,519
storage have to be accessed by by BIOS

60
00:02:44,340 --> 00:02:51,239
code then it have to be relatively

61
00:02:47,519 --> 00:02:54,080
simple to be accessed at very early boot

62
00:02:51,239 --> 00:02:57,599
stage so initialization of that Hardware

63
00:02:54,080 --> 00:02:59,459
cannot be too too complex since storage

64
00:02:57,599 --> 00:03:02,640
with such capabilities may be

65
00:02:59,459 --> 00:03:04,739
constrained we we should use variables

66
00:03:02,640 --> 00:03:07,739
only other means of configuration

67
00:03:04,739 --> 00:03:09,840
communication cannot be used so what we

68
00:03:07,739 --> 00:03:12,060
mean by other means of configuration

69
00:03:09,840 --> 00:03:14,819
communication so for example a system

70
00:03:12,060 --> 00:03:16,860
tables like ACPI tables SMBIOS tables

71
00:03:14,819 --> 00:03:19,800
those can be also used for communication

72
00:03:16,860 --> 00:03:22,140
between film and operating system but

73
00:03:19,800 --> 00:03:24,840
last resort we have you have five

74
00:03:22,140 --> 00:03:27,239
variables variables are typically stored

75
00:03:24,840 --> 00:03:29,819
in dedicated area of firmware device

76
00:03:27,239 --> 00:03:32,519
which consists typically coupled

77
00:03:29,819 --> 00:03:34,920
firmware volumes and typically one of

78
00:03:32,519 --> 00:03:37,860
these firmware volumes has subtype

79
00:03:34,920 --> 00:03:40,500
subtype and vram and that's the place

80
00:03:37,860 --> 00:03:43,080
where the where the UEFI variables are

81
00:03:40,500 --> 00:03:46,860
stored the core of variables is to

82
00:03:43,080 --> 00:03:49,319
provide all the required information for

83
00:03:46,860 --> 00:03:52,140
UEFI compatible system components like

84
00:03:49,319 --> 00:03:55,080
like boot firmware and bootloaders

85
00:03:52,140 --> 00:03:57,120
operating system UEFI applications but

86
00:03:55,080 --> 00:03:59,120
recently there is even work for example

87
00:03:57,120 --> 00:04:03,060
in corporate which is not UEFI compatible

88
00:03:59,120 --> 00:04:05,819
to also support UEFI variables so other

89
00:04:03,060 --> 00:04:08,480
and other components also consider UEFI

90
00:04:05,819 --> 00:04:11,459
variables supporting UEFI variables

91
00:04:08,480 --> 00:04:14,640
specification of services related to

92
00:04:11,459 --> 00:04:16,680
handling qfi variables are part of a

93
00:04:14,640 --> 00:04:19,380
runtime Services section in UEFI

94
00:04:16,680 --> 00:04:23,580
specification so detailed description

95
00:04:19,380 --> 00:04:26,160
can be found in chapter 8.2 and final

96
00:04:23,580 --> 00:04:28,020
final information is that knowledge of

97
00:04:26,160 --> 00:04:30,240
UEFI variables is critical to

98
00:04:28,020 --> 00:04:33,120
understanding UEFI secure boot

99
00:04:30,240 --> 00:04:35,220
Technologies it's Technologies so please

100
00:04:33,120 --> 00:04:37,199
pay attention to the because next

101
00:04:35,220 --> 00:04:42,259
sections those will be critical to

102
00:04:37,199 --> 00:04:42,259
understand how UEFI secure boot works

