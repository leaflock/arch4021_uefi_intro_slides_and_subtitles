1
00:00:00,299 --> 00:00:05,640
but what protection machines exist to

2
00:00:02,879 --> 00:00:08,160
defend our UEFI variables there are

3
00:00:05,640 --> 00:00:11,219
multiple protection mechanisms which we

4
00:00:08,160 --> 00:00:13,940
can mainly divide into two section pair

5
00:00:11,219 --> 00:00:17,699
attack resistance there are software

6
00:00:13,940 --> 00:00:20,160
attack resistance protection mechanism

7
00:00:17,699 --> 00:00:22,980
and Hardware attack resistance

8
00:00:20,160 --> 00:00:25,439
protection mechanism we would like to

9
00:00:22,980 --> 00:00:28,619
not talk too much in about Hardware

10
00:00:25,439 --> 00:00:32,040
attack protection mechanism just to

11
00:00:28,619 --> 00:00:34,340
mention couple on this slide software

12
00:00:32,040 --> 00:00:37,739
protection mechanisms will be exploited

13
00:00:34,340 --> 00:00:38,700
more widely in further part of the of

14
00:00:37,739 --> 00:00:41,100
the course

15
00:00:38,700 --> 00:00:44,640
so let's start with Hardware Integrity

16
00:00:41,100 --> 00:00:46,920
protection mechanism first is reply

17
00:00:44,640 --> 00:00:49,200
protect memory block partition which is

18
00:00:46,920 --> 00:00:52,320
supported by some Hardware devices like

19
00:00:49,200 --> 00:00:56,820
nvme Universal Universal flash storage

20
00:00:52,320 --> 00:00:59,280
or emmc so this rpmb is typically used

21
00:00:56,820 --> 00:01:02,399
in combination with trusted execution

22
00:00:59,280 --> 00:01:04,979
environment it works in a way that that

23
00:01:02,399 --> 00:01:09,380
during the manufacturing process of the

24
00:01:04,979 --> 00:01:11,939
storage there is rpmb key generated

25
00:01:09,380 --> 00:01:15,420
which is fused into one-time

26
00:01:11,939 --> 00:01:18,240
programmable fuses and this key can be

27
00:01:15,420 --> 00:01:22,140
then used to access data which are

28
00:01:18,240 --> 00:01:24,119
already in rpmb Partition and this key

29
00:01:22,140 --> 00:01:26,820
can be used only from The Trusted

30
00:01:24,119 --> 00:01:31,140
execution environment to even improve

31
00:01:26,820 --> 00:01:33,540
the security of rpmb there is also a

32
00:01:31,140 --> 00:01:38,100
reply protected monotonic counter which

33
00:01:33,540 --> 00:01:42,420
also has its own key fused in OTP and it

34
00:01:38,100 --> 00:01:44,280
is used to to avoid reply attacks other

35
00:01:42,420 --> 00:01:46,500
means which can be used to protect

36
00:01:44,280 --> 00:01:48,299
against Hardware attacks and like

37
00:01:46,500 --> 00:01:51,060
protecting our UEFI variables against

38
00:01:48,299 --> 00:01:55,200
Hardware attacks is storing those UEFI

39
00:01:51,060 --> 00:01:59,280
variables in TPM CSME and in Hardware

40
00:01:55,200 --> 00:02:01,979
support Hardware security module or

41
00:01:59,280 --> 00:02:04,560
other type of Secure Storage so

42
00:02:01,979 --> 00:02:06,600
interestingly UEFI designers in the

43
00:02:04,560 --> 00:02:08,940
specification considered such use case

44
00:02:06,600 --> 00:02:12,360
and and we will talk about that later

45
00:02:08,940 --> 00:02:14,280
despite those early met in in real life

46
00:02:12,360 --> 00:02:16,080
from the availability protection

47
00:02:14,280 --> 00:02:19,280
Hardware availability protection

48
00:02:16,080 --> 00:02:22,319
mechanism we have two here first is

49
00:02:19,280 --> 00:02:24,860
atomicity it is guaranteed by the

50
00:02:22,319 --> 00:02:28,560
hardware uh because nor flash designs

51
00:02:24,860 --> 00:02:31,980
guarantee atomicity of of writing one

52
00:02:28,560 --> 00:02:34,200
byte but of course to write whole UEFI

53
00:02:31,980 --> 00:02:37,200
variable we need the implementation of

54
00:02:34,200 --> 00:02:40,020
correct I update flow which is already

55
00:02:37,200 --> 00:02:43,920
part of EDK2 implementation so it kind

56
00:02:40,020 --> 00:02:46,560
of works for us in in background by

57
00:02:43,920 --> 00:02:49,980
default there is also for tolerant right

58
00:02:46,560 --> 00:02:53,160
so it may happen that that attacker

59
00:02:49,980 --> 00:02:57,060
might try to do power loss in the middle

60
00:02:53,160 --> 00:03:00,060
of the UEFI variable update to to break

61
00:02:57,060 --> 00:03:03,060
our security and it is already

62
00:03:00,060 --> 00:03:06,000
implemented also in EDK2 as a driver but

63
00:03:03,060 --> 00:03:10,200
it does it's a separate driver which

64
00:03:06,000 --> 00:03:12,360
have to be enabled to to to work in

65
00:03:10,200 --> 00:03:15,000
default compilation what what it does

66
00:03:12,360 --> 00:03:19,040
with it what this photo and Driver

67
00:03:15,000 --> 00:03:23,540
really does is just tracking precisely

68
00:03:19,040 --> 00:03:26,760
what right transaction sent to our

69
00:03:23,540 --> 00:03:30,720
SPI or to other storage that stores UEFI

70
00:03:26,760 --> 00:03:33,840
variables are completed and only based

71
00:03:30,720 --> 00:03:36,480
on on that decide if the whole variable

72
00:03:33,840 --> 00:03:39,420
was written or not and then if something

73
00:03:36,480 --> 00:03:42,540
happened in the middle we are sure that

74
00:03:39,420 --> 00:03:45,780
not whole variable was written to the

75
00:03:42,540 --> 00:03:48,360
storage and last technique is used for

76
00:03:45,780 --> 00:03:51,480
confident confidentiality protection for

77
00:03:48,360 --> 00:03:53,480
Hardware confidentiality protection

78
00:03:51,480 --> 00:03:58,140
um this technique helps

79
00:03:53,480 --> 00:04:00,560
helps helps us on not only storing the

80
00:03:58,140 --> 00:04:03,480
variables as in case of

81
00:04:00,560 --> 00:04:07,799
Integrity protection not only storing in

82
00:04:03,480 --> 00:04:11,580
TPM CSME or or other protected storage

83
00:04:07,799 --> 00:04:13,500
but storing encrypted version of the UEFI

84
00:04:11,580 --> 00:04:17,160
variables so the question is how we

85
00:04:13,500 --> 00:04:19,739
decrypt those encrypted variable we can

86
00:04:17,160 --> 00:04:21,900
decrypt it either by user means which

87
00:04:19,739 --> 00:04:24,840
means providing password during boot or

88
00:04:21,900 --> 00:04:28,020
some fingerprint or some USB token or by

89
00:04:24,840 --> 00:04:32,220
platform means which can be for example

90
00:04:28,020 --> 00:04:35,360
a secret which is unsealed from the TPM

91
00:04:32,220 --> 00:04:38,100
after pcrs in TPM will be correctly

92
00:04:35,360 --> 00:04:40,740
populated yeah and that's it from from

93
00:04:38,100 --> 00:04:44,280
Hardware perspective and now we can talk

94
00:04:40,740 --> 00:04:47,120
about resisting software attacks and and

95
00:04:44,280 --> 00:04:49,979
protecting Integrity availability and

96
00:04:47,120 --> 00:04:51,419
confidentiality using software attack

97
00:04:49,979 --> 00:04:54,840
resistance

98
00:04:51,419 --> 00:04:57,960
so let's start with Mahoney's that can

99
00:04:54,840 --> 00:05:01,380
help us resist software attacks only

100
00:04:57,960 --> 00:05:03,840
Wi-Fi variables and and let's start with

101
00:05:01,380 --> 00:05:06,720
a confidentiality protection we already

102
00:05:03,840 --> 00:05:10,080
discussed that as a part of the hardware

103
00:05:06,720 --> 00:05:12,960
part of the protection but the point

104
00:05:10,080 --> 00:05:16,320
here is that also the software part so

105
00:05:12,960 --> 00:05:19,940
the way that user provides the password

106
00:05:16,320 --> 00:05:22,320
or the the content for generating key

107
00:05:19,940 --> 00:05:26,039
also have to be protected and of course

108
00:05:22,320 --> 00:05:28,680
that's up to the implementer in the in

109
00:05:26,039 --> 00:05:31,080
the UF file then let's jump to the

110
00:05:28,680 --> 00:05:34,380
availability protection because

111
00:05:31,080 --> 00:05:38,039
availability protection is built in EDK2

112
00:05:34,380 --> 00:05:40,919
and and we can say it's technology just

113
00:05:38,039 --> 00:05:43,800
would just work in in background we can

114
00:05:40,919 --> 00:05:46,979
say that this this can be safely omitted

115
00:05:43,800 --> 00:05:49,560
and we will not talk much about that of

116
00:05:46,979 --> 00:05:51,660
course both flashware out protection as

117
00:05:49,560 --> 00:05:53,759
well as quota management

118
00:05:51,660 --> 00:05:55,979
implementations could be interesting

119
00:05:53,759 --> 00:06:00,060
ideas for researchers looking for

120
00:05:55,979 --> 00:06:02,520
vulnerabilities but those are not meant

121
00:06:00,060 --> 00:06:05,039
to be used by firmware developers in in

122
00:06:02,520 --> 00:06:07,740
kind of regular everyday development

123
00:06:05,039 --> 00:06:09,240
workflow and and we think that Integrity

124
00:06:07,740 --> 00:06:12,060
protection and and variable

125
00:06:09,240 --> 00:06:16,139
authentication is is way more important

126
00:06:12,060 --> 00:06:18,600
and we often use it so let's let's go to

127
00:06:16,139 --> 00:06:21,960
Integrity protection mechanism

128
00:06:18,600 --> 00:06:25,020
as the means of Defending against

129
00:06:21,960 --> 00:06:27,960
software attacks so Main and most used

130
00:06:25,020 --> 00:06:30,479
and most popular and and the one which

131
00:06:27,960 --> 00:06:33,720
will be widely discussed in this course

132
00:06:30,479 --> 00:06:36,479
is variable authentication this behind

133
00:06:33,720 --> 00:06:40,620
this is for ensuring that entity which

134
00:06:36,479 --> 00:06:43,800
calls UEFI runtime Services function set

135
00:06:40,620 --> 00:06:46,400
variables which is used for setting the

136
00:06:43,800 --> 00:06:49,699
and updating the content of the variable

137
00:06:46,400 --> 00:06:52,380
has the authority to really do that

138
00:06:49,699 --> 00:06:55,620
operation there are various

139
00:06:52,380 --> 00:06:58,080
authentication mechanisms to prove that

140
00:06:55,620 --> 00:07:01,319
and we will discuss those authentication

141
00:06:58,080 --> 00:07:03,360
machines on the next slides

142
00:07:01,319 --> 00:07:06,680
um the second Integrity protection

143
00:07:03,360 --> 00:07:08,880
mechanism is is accessing and updating

144
00:07:06,680 --> 00:07:12,300
variables through trusted execution

145
00:07:08,880 --> 00:07:14,699
environment which in case of x86 is a

146
00:07:12,300 --> 00:07:18,539
system management mode and it's just it

147
00:07:14,699 --> 00:07:21,660
works in a way and the belief is that if

148
00:07:18,539 --> 00:07:24,560
we do the access to the variables in

149
00:07:21,660 --> 00:07:27,419
tightly controlled and isolated

150
00:07:24,560 --> 00:07:29,940
environment it improves protection of

151
00:07:27,419 --> 00:07:32,280
course this method is fine as long as As

152
00:07:29,940 --> 00:07:35,699
We like a system management mode and and

153
00:07:32,280 --> 00:07:37,620
we we trust in in system management mode

154
00:07:35,699 --> 00:07:40,440
implementation of there are some

155
00:07:37,620 --> 00:07:43,860
platforms which does not support us SMM

156
00:07:40,440 --> 00:07:47,340
and there are some designs and thread

157
00:07:43,860 --> 00:07:51,479
thread models we just don't want SMM to

158
00:07:47,340 --> 00:07:53,880
be part of the of the platform

159
00:07:51,479 --> 00:07:56,340
so in such case implementation which is

160
00:07:53,880 --> 00:07:58,560
already done in EDK2 and is default

161
00:07:56,340 --> 00:07:59,220
implementation to handle

162
00:07:58,560 --> 00:08:03,780
um

163
00:07:59,220 --> 00:08:06,180
UEFI variables updates cannot be used so

164
00:08:03,780 --> 00:08:10,319
someone have to implement his own other

165
00:08:06,180 --> 00:08:13,620
mahalism is lock or variable log it may

166
00:08:10,319 --> 00:08:16,560
happen that some variables should not be

167
00:08:13,620 --> 00:08:19,340
changed after some boot stage so

168
00:08:16,560 --> 00:08:22,500
typically those variables are like

169
00:08:19,340 --> 00:08:25,440
configuration related to memory training

170
00:08:22,500 --> 00:08:29,879
or for example some stuff related to

171
00:08:25,440 --> 00:08:32,880
system management mode memory and those

172
00:08:29,879 --> 00:08:35,940
configuration options or configuration

173
00:08:32,880 --> 00:08:38,940
parameters can be locked and after those

174
00:08:35,940 --> 00:08:42,240
will be locked they cannot be changed

175
00:08:38,940 --> 00:08:44,580
and until reboot finally there are also

176
00:08:42,240 --> 00:08:47,880
sanity checks sanity checks help make

177
00:08:44,580 --> 00:08:50,040
sure that variable can get only valid

178
00:08:47,880 --> 00:08:52,260
value typically this is done in BIOS

179
00:08:50,040 --> 00:08:56,220
setup menu when we go to setup menu and

180
00:08:52,260 --> 00:09:00,959
want to set given value give some data

181
00:08:56,220 --> 00:09:03,420
to to do given variable and then BIOS

182
00:09:00,959 --> 00:09:05,640
setup menu validates if this is a

183
00:09:03,420 --> 00:09:07,800
correct value if this is in range if

184
00:09:05,640 --> 00:09:12,060
this is not below minimum or over

185
00:09:07,800 --> 00:09:14,760
Maximum and so on if if it is then it

186
00:09:12,060 --> 00:09:17,459
does not allow us update the variable in

187
00:09:14,760 --> 00:09:19,800
such way and in this in this course we

188
00:09:17,459 --> 00:09:22,800
mostly focusing as I said on variable

189
00:09:19,800 --> 00:09:26,519
authentication and we will also talk a

190
00:09:22,800 --> 00:09:26,519
little bit about updating

