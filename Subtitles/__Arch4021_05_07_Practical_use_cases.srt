1
00:00:00,000 --> 00:00:04,560
we already know how variables are

2
00:00:02,760 --> 00:00:07,319
created and updated based on

3
00:00:04,560 --> 00:00:09,300
authentication 2 and 3 flows explained

4
00:00:07,319 --> 00:00:11,160
in previous sections but let's look into

5
00:00:09,300 --> 00:00:14,099
process of verification which happened

6
00:00:11,160 --> 00:00:16,800
every time when we try to create or

7
00:00:14,099 --> 00:00:20,100
update update the variable so to some

8
00:00:16,800 --> 00:00:23,600
extent we can say that color of set

9
00:00:20,100 --> 00:00:28,560
variable function handles authentication

10
00:00:23,600 --> 00:00:33,780
so the caller assumes or or claims that

11
00:00:28,560 --> 00:00:36,120
data color provide is authentic but a

12
00:00:33,780 --> 00:00:40,920
set variable is used to perform

13
00:00:36,120 --> 00:00:44,399
authorization so it checks if color is

14
00:00:40,920 --> 00:00:47,219
authorized to write to given variable so

15
00:00:44,399 --> 00:00:49,079
what are the checks performed when when

16
00:00:47,219 --> 00:00:50,420
we're using time-based authenticated

17
00:00:49,079 --> 00:00:54,140
right access

18
00:00:50,420 --> 00:00:59,219
attribute then like first thing is

19
00:00:54,140 --> 00:01:01,860
verification of of format of data then

20
00:00:59,219 --> 00:01:04,559
there is very verification of timestamp

21
00:01:01,860 --> 00:01:07,500
it is checked that if the timestamp is

22
00:01:04,559 --> 00:01:09,979
later then current value assigned to to

23
00:01:07,500 --> 00:01:13,320
the variable if there is any value

24
00:01:09,979 --> 00:01:16,799
assigned then there is check if the

25
00:01:13,320 --> 00:01:19,680
signature if the harsh signature is

26
00:01:16,799 --> 00:01:21,900
correct and at this time both data and

27
00:01:19,680 --> 00:01:24,680
signature from set variable input is

28
00:01:21,900 --> 00:01:27,720
used for verification then

29
00:01:24,680 --> 00:01:30,780
there is check if Target valuable can be

30
00:01:27,720 --> 00:01:33,360
changed by the signer and which signed

31
00:01:30,780 --> 00:01:35,700
the the hash provided in set variable

32
00:01:33,360 --> 00:01:38,420
during the check there are a lot of

33
00:01:35,700 --> 00:01:42,060
special cases which depends on the

34
00:01:38,420 --> 00:01:44,220
variable type and there's there are

35
00:01:42,060 --> 00:01:45,960
different rules some may be even

36
00:01:44,220 --> 00:01:49,140
consider it as back doors as we

37
00:01:45,960 --> 00:01:52,680
explained for example up and right can

38
00:01:49,140 --> 00:01:56,700
be can be used in some rare cases there

39
00:01:52,680 --> 00:02:02,280
are also secure boot variables which in

40
00:01:56,700 --> 00:02:05,280
setup model can be written only if some

41
00:02:02,280 --> 00:02:07,740
dummy authentication to descriptor is

42
00:02:05,280 --> 00:02:11,039
provided but those are some special

43
00:02:07,740 --> 00:02:13,379
cases used for setting up new variables

44
00:02:11,039 --> 00:02:17,220
if variable doesn't belong to any of

45
00:02:13,379 --> 00:02:18,440
special special classes known to a UEFI

46
00:02:17,220 --> 00:02:21,300
implementation

47
00:02:18,440 --> 00:02:23,940
we can call those those variables

48
00:02:21,300 --> 00:02:26,760
private authenticated variables so those

49
00:02:23,940 --> 00:02:29,459
are user private authenticated variables

50
00:02:26,760 --> 00:02:34,319
which user can handle with his own

51
00:02:29,459 --> 00:02:36,540
signature with his own Keys yes and such

52
00:02:34,319 --> 00:02:39,420
such variables obviously can be changed

53
00:02:36,540 --> 00:02:42,480
only by the owner of the of the key if

54
00:02:39,420 --> 00:02:43,879
any of the tracks format of data

55
00:02:42,480 --> 00:02:46,700
timestamp

56
00:02:43,879 --> 00:02:50,160
validity of the signature

57
00:02:46,700 --> 00:02:52,680
fail then there is EFI security

58
00:02:50,160 --> 00:02:55,040
violation error returned by said

59
00:02:52,680 --> 00:02:55,040
valuable

