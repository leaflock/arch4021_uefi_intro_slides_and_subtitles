1
00:00:00,000 --> 00:00:04,920
let's plug this exploration of your five

2
00:00:02,820 --> 00:00:06,779
field Market hierarchical structures in

3
00:00:04,920 --> 00:00:10,139
this practice we would like to use ufi

4
00:00:06,779 --> 00:00:12,179
tool and optionally ufi extract both

5
00:00:10,139 --> 00:00:14,940
tools have the same purpose but first

6
00:00:12,179 --> 00:00:17,400
one using graphical user interface and

7
00:00:14,940 --> 00:00:19,440
second one using command line normally

8
00:00:17,400 --> 00:00:22,680
we would start with downloading

9
00:00:19,440 --> 00:00:24,840
executable but just to save time and

10
00:00:22,680 --> 00:00:28,260
network bandwidth we have already ufi

11
00:00:24,840 --> 00:00:31,080
tool and ufi extract in our course

12
00:00:28,260 --> 00:00:33,960
virtual machine and just for reference

13
00:00:31,080 --> 00:00:36,780
we include installation instruction in

14
00:00:33,960 --> 00:00:41,420
the course materials so let's start with

15
00:00:36,780 --> 00:00:41,420
running qfi tool in in terminal

16
00:00:49,260 --> 00:00:59,039
okay so uh as you can see ufi tool

17
00:00:54,840 --> 00:01:01,620
um got regular main menu file actions

18
00:00:59,039 --> 00:01:05,159
view help we will overview what's what's

19
00:01:01,620 --> 00:01:07,380
inside of those during this exercise in

20
00:01:05,159 --> 00:01:09,600
addition to that we have a couple other

21
00:01:07,380 --> 00:01:12,479
windows which can deliver some

22
00:01:09,600 --> 00:01:15,720
information when we will open ufi image

23
00:01:12,479 --> 00:01:20,100
so there is a structure window which is

24
00:01:15,720 --> 00:01:21,720
uh which would be mostly used by US

25
00:01:20,100 --> 00:01:24,840
um and we will see how it looks like

26
00:01:21,720 --> 00:01:26,159
when we load some firmware image and

27
00:01:24,840 --> 00:01:28,259
then there are there are information

28
00:01:26,159 --> 00:01:30,540
when we click on the components we will

29
00:01:28,259 --> 00:01:33,240
see what's going on and there are some

30
00:01:30,540 --> 00:01:35,340
status Windows related to various parset

31
00:01:33,240 --> 00:01:39,360
structures on the bottom

32
00:01:35,340 --> 00:01:40,320
so let's start with loading uh sample

33
00:01:39,360 --> 00:01:45,439
image

34
00:01:40,320 --> 00:01:49,200
we should have it in edk2 build a vmf

35
00:01:45,439 --> 00:01:55,340
x64 debug GCC

36
00:01:49,200 --> 00:01:58,740
um feel my volume and ovmf FD

37
00:01:55,340 --> 00:02:01,820
so as you can see the ufi tool

38
00:01:58,740 --> 00:02:06,119
recognized our image as a ufi image

39
00:02:01,820 --> 00:02:08,940
Type image with subtype ufi there are

40
00:02:06,119 --> 00:02:10,979
various image types which wi-fi tool can

41
00:02:08,940 --> 00:02:13,680
recognize we will see we will take a

42
00:02:10,979 --> 00:02:16,560
look at at others to show some features

43
00:02:13,680 --> 00:02:19,620
of ufi tool okay so let's look at file

44
00:02:16,560 --> 00:02:22,560
money and what kind of options we have

45
00:02:19,620 --> 00:02:24,840
here so we can also open a file in

46
00:02:22,560 --> 00:02:28,080
another window we can generate text

47
00:02:24,840 --> 00:02:31,860
report which will contain very detailed

48
00:02:28,080 --> 00:02:33,620
information about our image we can also

49
00:02:31,860 --> 00:02:35,700
load

50
00:02:33,620 --> 00:02:39,300
guid database

51
00:02:35,700 --> 00:02:42,720
so this option gives us ability to load

52
00:02:39,300 --> 00:02:45,540
some custom database which contains good

53
00:02:42,720 --> 00:02:47,940
that we identified here in our previous

54
00:02:45,540 --> 00:02:50,220
research so we don't have that file here

55
00:02:47,940 --> 00:02:52,440
but maybe in you in your own research

56
00:02:50,220 --> 00:02:54,660
we'll create such file or you will have

57
00:02:52,440 --> 00:02:56,459
file because you will work with some

58
00:02:54,660 --> 00:02:58,860
firmware developers who can provide you

59
00:02:56,459 --> 00:03:01,860
information about the goods used in the

60
00:02:58,860 --> 00:03:04,800
firmware image that you exploding there

61
00:03:01,860 --> 00:03:08,220
is also a default guide database

62
00:03:04,800 --> 00:03:11,879
included in ufi tool it contains around

63
00:03:08,220 --> 00:03:15,300
4 000 good identifiers those are like

64
00:03:11,879 --> 00:03:17,819
well known good identifiers reported by

65
00:03:15,300 --> 00:03:20,819
various researchers to the ufi tool

66
00:03:17,819 --> 00:03:23,400
projects reported by authors plus those

67
00:03:20,819 --> 00:03:27,599
which are very well known because those

68
00:03:23,400 --> 00:03:30,300
exist in open source edk2 so they added

69
00:03:27,599 --> 00:03:33,720
it for for simply simplification we will

70
00:03:30,300 --> 00:03:36,599
see how loading default Goods help us

71
00:03:33,720 --> 00:03:38,879
understand the content of your file

72
00:03:36,599 --> 00:03:42,180
image click it also unload the database

73
00:03:38,879 --> 00:03:44,879
and we can export discovered guides what

74
00:03:42,180 --> 00:03:48,840
this option does it just creates a comma

75
00:03:44,879 --> 00:03:51,900
separated value file so CSV file which

76
00:03:48,840 --> 00:03:54,120
contain guid and that text which was

77
00:03:51,900 --> 00:03:57,360
associated with given guid if there was

78
00:03:54,120 --> 00:03:58,620
any found in the in the file

79
00:03:57,360 --> 00:04:02,340
so

80
00:03:58,620 --> 00:04:06,480
um so first let's look like how how our

81
00:04:02,340 --> 00:04:09,239
loading of good works so you can see

82
00:04:06,480 --> 00:04:11,879
that the default guides were already

83
00:04:09,239 --> 00:04:16,440
loaded because this guide is already ENT

84
00:04:11,879 --> 00:04:19,799
fight like as a EFI system NV data

85
00:04:16,440 --> 00:04:22,260
firmware volume guide if we go deeper we

86
00:04:19,799 --> 00:04:24,900
can find also some some goods were not

87
00:04:22,260 --> 00:04:27,720
identified but here we can find the

88
00:04:24,900 --> 00:04:30,900
guide which was hidden file as a lcma

89
00:04:27,720 --> 00:04:32,940
custom the compressed grid which is

90
00:04:30,900 --> 00:04:36,240
related with the compression

91
00:04:32,940 --> 00:04:38,060
so the code to use it and here we see

92
00:04:36,240 --> 00:04:42,300
there is more

93
00:04:38,060 --> 00:04:44,160
stuff identified related with pi when we

94
00:04:42,300 --> 00:04:48,139
unload

95
00:04:44,160 --> 00:04:48,139
those those guides

96
00:04:48,660 --> 00:04:55,080
we can see that

97
00:04:50,759 --> 00:04:58,820
our NV Ram is not identified and when we

98
00:04:55,080 --> 00:05:03,180
go deeper like more stuff is is on

99
00:04:58,820 --> 00:05:05,580
identified just plain goods and we don't

100
00:05:03,180 --> 00:05:08,520
know what exactly here inside of course

101
00:05:05,580 --> 00:05:12,000
we can learn a little bit uh using this

102
00:05:08,520 --> 00:05:14,479
text but not all files in the system

103
00:05:12,000 --> 00:05:17,520
image will contain that if we explore

104
00:05:14,479 --> 00:05:19,860
this structure window when we click on

105
00:05:17,520 --> 00:05:22,020
various components we can see that the

106
00:05:19,860 --> 00:05:23,240
on the right side there are some

107
00:05:22,020 --> 00:05:26,100
detailed information

108
00:05:23,240 --> 00:05:28,560
associated with the

109
00:05:26,100 --> 00:05:32,180
with given components in this case this

110
00:05:28,560 --> 00:05:36,120
is volume so it has one type of set of

111
00:05:32,180 --> 00:05:40,860
parameters attributes if we go to let's

112
00:05:36,120 --> 00:05:43,380
say a file the set of parameters is

113
00:05:40,860 --> 00:05:45,840
different of course we will not go into

114
00:05:43,380 --> 00:05:47,660
details of what every every of this

115
00:05:45,840 --> 00:05:50,340
parameter means some are

116
00:05:47,660 --> 00:05:54,660
self-explanatory like checksums or body

117
00:05:50,340 --> 00:05:57,660
or header or offsets but some are have

118
00:05:54,660 --> 00:06:00,840
more meaning and what is important is

119
00:05:57,660 --> 00:06:03,660
that there is action menu and depending

120
00:06:00,840 --> 00:06:06,060
on the type of component that is

121
00:06:03,660 --> 00:06:08,580
selected in the structure window various

122
00:06:06,060 --> 00:06:12,600
options are available so for example in

123
00:06:08,580 --> 00:06:13,580
this case we can have hex view of the

124
00:06:12,600 --> 00:06:18,120
image

125
00:06:13,580 --> 00:06:20,280
or or we can extract it to the to the

126
00:06:18,120 --> 00:06:22,979
file

127
00:06:20,280 --> 00:06:24,539
then if we choose different components

128
00:06:22,979 --> 00:06:27,240
let's say event section

129
00:06:24,539 --> 00:06:28,740
then we have section highlighted and

130
00:06:27,240 --> 00:06:31,080
there are different set of options

131
00:06:28,740 --> 00:06:34,380
available for us

132
00:06:31,080 --> 00:06:35,360
and some are even hidden because of

133
00:06:34,380 --> 00:06:38,580
um

134
00:06:35,360 --> 00:06:40,080
not working with the stock of image or

135
00:06:38,580 --> 00:06:43,560
this type of file

136
00:06:40,080 --> 00:06:46,680
the same thing for the files and we see

137
00:06:43,560 --> 00:06:49,740
the file is highlighted so

138
00:06:46,680 --> 00:06:52,680
um the same menu is also available as a

139
00:06:49,740 --> 00:06:55,199
context menu so you can see that if I

140
00:06:52,680 --> 00:06:57,539
click right click on the component I

141
00:06:55,199 --> 00:06:59,039
have the same actions which are visible

142
00:06:57,539 --> 00:07:03,120
here

143
00:06:59,039 --> 00:07:06,539
when we load uh Intel boot like when we

144
00:07:03,120 --> 00:07:10,800
load image which which is four Intel

145
00:07:06,539 --> 00:07:13,259
image with the firmware interface table

146
00:07:10,800 --> 00:07:16,860
inside

147
00:07:13,259 --> 00:07:19,740
um we can see even more so let's so I

148
00:07:16,860 --> 00:07:23,099
have couple images prepared here so if

149
00:07:19,740 --> 00:07:24,960
you will go to the home directory and

150
00:07:23,099 --> 00:07:28,139
choose all files

151
00:07:24,960 --> 00:07:33,660
you will find these two two files on the

152
00:07:28,139 --> 00:07:37,440
bottom z7t z17 and z77

153
00:07:33,660 --> 00:07:39,660
so let's load the z17

154
00:07:37,440 --> 00:07:44,520
and you can see that there are some

155
00:07:39,660 --> 00:07:47,160
colors appearing here on on in our image

156
00:07:44,520 --> 00:07:48,419
in our structure window and there are

157
00:07:47,160 --> 00:07:52,919
even

158
00:07:48,419 --> 00:07:55,680
um some data identified and parset and

159
00:07:52,919 --> 00:07:57,539
display it on the bottom so let's

160
00:07:55,680 --> 00:08:00,660
quickly go through that so everything

161
00:07:57,539 --> 00:08:04,139
here is this colors and those structures

162
00:08:00,660 --> 00:08:06,539
which were parsed are related with Intel

163
00:08:04,139 --> 00:08:08,880
boot Garden Technology so Intel wood

164
00:08:06,539 --> 00:08:11,000
guard is a security technology it's a

165
00:08:08,880 --> 00:08:14,120
root of trust technology

166
00:08:11,000 --> 00:08:17,039
which gives ability to fuse public key

167
00:08:14,120 --> 00:08:21,120
into processor which gives ability then

168
00:08:17,039 --> 00:08:25,020
to verify firmware images the process is

169
00:08:21,120 --> 00:08:27,120
way way more complex but for now you you

170
00:08:25,020 --> 00:08:28,740
just have to know that it's it's a

171
00:08:27,120 --> 00:08:31,020
security technology helping in

172
00:08:28,740 --> 00:08:34,020
verification of the firmware that's

173
00:08:31,020 --> 00:08:37,440
booting in our system what we can see

174
00:08:34,020 --> 00:08:40,200
here is that there are three colors in

175
00:08:37,440 --> 00:08:42,120
defined and of course Intel bootcard

176
00:08:40,200 --> 00:08:44,540
technology depending on the

177
00:08:42,120 --> 00:08:47,660
configuration which is partially

178
00:08:44,540 --> 00:08:51,720
displayed in firmware interface table

179
00:08:47,660 --> 00:08:55,740
showed in this type provides our ranges

180
00:08:51,720 --> 00:08:59,760
which protect parts of the firmware

181
00:08:55,740 --> 00:09:02,540
image so in this case the red range is a

182
00:08:59,760 --> 00:09:05,100
Intel boot guard initial boot block

183
00:09:02,540 --> 00:09:08,120
protected range so probably that's why

184
00:09:05,100 --> 00:09:13,140
it's red it cannot be modified

185
00:09:08,120 --> 00:09:15,300
at all cyan color code show that those

186
00:09:13,140 --> 00:09:17,519
are different protectant wrenches not

187
00:09:15,300 --> 00:09:20,600
related to Intel boot guard but may be

188
00:09:17,519 --> 00:09:24,320
related to protection

189
00:09:20,600 --> 00:09:27,019
set up by the independent bios vendor

190
00:09:24,320 --> 00:09:30,660
and the yellow one

191
00:09:27,019 --> 00:09:32,459
say that those ranges are just partially

192
00:09:30,660 --> 00:09:33,740
protected so those are not fully

193
00:09:32,459 --> 00:09:37,019
protected

194
00:09:33,740 --> 00:09:38,820
by the Intel boot guard technology Okay

195
00:09:37,019 --> 00:09:41,180
so so there are also there is also

196
00:09:38,820 --> 00:09:45,360
security Windows which cover

197
00:09:41,180 --> 00:09:47,220
content of Intel boot guard manifest and

198
00:09:45,360 --> 00:09:50,459
and various details related with

199
00:09:47,220 --> 00:09:52,380
internal wood guard technology okay and

200
00:09:50,459 --> 00:09:55,440
that's that that would be it is we can

201
00:09:52,380 --> 00:10:00,920
enable and disable those markings by

202
00:09:55,440 --> 00:10:00,920
just using view option bootcut markings

