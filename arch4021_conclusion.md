* Congratulations you get to the end of Arch4021 Introductory UEFI
* Recap what we learned
* Firmware fundamentals: terminology and history.
* How Legacy BIOS evolved into UEFI BIOS.
* UEFI and PI Specifications:
  - Goals,
  - Features,
  - Architecture,
  - Implementations.
* How to build and debug EDKII.
* How to explore UEFI BIOS images.
* How to boot UEFI BIOS in QEMU.
* How UEFI BIOS boot process look like.
* How to use UEFI Shell.
* How UEFI BIOS configuration is stored and what are the security properties of
  UEFI Variables.
* Thank you, please share and good luck.
